<?php
    require_once "../lib/config.php";
    if (!userLogged()) {
?>
<?php
}
?>
<aside class="bg-dark">
    <center>
        <blockquote style="max-width: 600px;">
            <p>Pillola azzurra, fine della storia: domani ti sveglierai in camera tua, e crederai a quello che vorrai. Pillola rossa, resti nel paese delle meraviglie, e vedrai quant'è profonda la tana del bianconiglio. Ti sto offrendo solo la verità, ricordalo. Niente di più.</p>
            <footer>Morpheus, The Matrix</footer>
        </blockquote>
        <br>
    	<a target="_blank" style="color:white;" href="https://twitter.com/redpill_co">Twitter</a>
		<a target="_blank" style="color:white;" href="https://www.facebook.com/redpill2">Facebook</a>
    	<br>
    	<a target="_blank" style="color:white;" href="privacy-policy.html">Privacy Policy</a>
    	 - Redpill © 2015
	 </center>
</aside>