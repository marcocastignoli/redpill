-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Creato il: Mag 14, 2018 alle 12:22
-- Versione del server: 10.0.34-MariaDB-0ubuntu0.16.04.1
-- Versione PHP: 7.1.17-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `friends`
--

DELIMITER $$
--
-- Funzioni
--
CREATE DEFINER=`Envious`@`localhost` FUNCTION `GeoDistDiff` (`type` ENUM('mi','km'), `lat1` DECIMAL(10,7), `lon1` DECIMAL(10,7), `lat2` DECIMAL(10,7), `lon2` DECIMAL(10,7)) RETURNS DECIMAL(10,7) BEGIN
  RETURN ( IF(type = 'km', 6371, 3959) * acos( cos( radians(lat2) ) * cos( radians( lat1 ) ) * cos( radians( lon1 ) - radians(lon2) ) + sin( radians(lat2) ) * sin( radians( lat1 ) ) ) );
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struttura della tabella `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `author` text NOT NULL,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `place` text NOT NULL,
  `date` text NOT NULL,
  `max` text NOT NULL,
  `category` text NOT NULL,
  `type` text NOT NULL,
  `photo` text NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `local` text NOT NULL,
  `col1` text NOT NULL,
  `col2` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `events`
--

INSERT INTO `events` (`id`, `author`, `title`, `description`, `place`, `date`, `max`, `category`, `type`, `photo`, `latitude`, `longitude`, `local`, `col1`, `col2`) VALUES
(3, '1', 'asd', 'asd', 'Experience Piacenza, Piazza Cavalli, 7, 29121 Piacenza PC, Italia', '2018-05-16 21:00:00', '0', '0', '0', 'img/events/3.jpg', '45.0526206', '9.692984499999966', 'Casa mia', '#0066CC', '#000000'),
(4, '1', 'Ventian Snares', 'Evento bellissimo XDXD', 'Via Coglialegna, 1-15, 29121 Piacenza PC, Italy', '2016-12-03 21:00:00', '0', '5', '0', '', '45.0529936', '9.687466699999959', 'Casa J T', '#f05f40', 'black'),
(5, '1', 'GODO', 'asd', 'Viale Palmerio Raimondo, 2-4, 29121 Piacenza PC, Italy', '2016-12-03 21:00:00', '0', '0', '0', 'img/events/5.gif', '45.04742279999999', '9.686568699999953', 'asd', '#424153', '#993399');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `hash` text NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `link` text NOT NULL,
  `fa` varchar(255) NOT NULL,
  `active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struttura della tabella `requests`
--

CREATE TABLE `requests` (
  `id` int(11) NOT NULL,
  `id_event` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `requests`
--

INSERT INTO `requests` (`id`, `id_event`, `id_user`, `status`) VALUES
(2, 3, 1, '1'),
(3, 5, 1, '1');

-- --------------------------------------------------------

--
-- Struttura della tabella `rides`
--

CREATE TABLE `rides` (
  `id_event` int(11) NOT NULL,
  `author` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `max` int(11) NOT NULL,
  `place` varchar(255) NOT NULL,
  `latitude` text NOT NULL,
  `longitude` text NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `rides`
--

INSERT INTO `rides` (`id_event`, `author`, `phone`, `max`, `place`, `latitude`, `longitude`, `id`) VALUES
(3, 1, '3341129020', 3, 'Centro Storico, 29121 Piacenza PC, Italia', '45.0527223', '9.687531600000057', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` text NOT NULL,
  `password` text NOT NULL,
  `name` text NOT NULL,
  `surname` text NOT NULL,
  `place` text NOT NULL,
  `longitude` text NOT NULL,
  `latitude` text NOT NULL,
  `email` text NOT NULL,
  `premium` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `surname`, `place`, `longitude`, `latitude`, `email`, `premium`) VALUES
(1, 'marcocastignoli', '8cb2237d0679ca88db6464eac60da96345513964', 'Marco', 'Castignoli', 'Experience Piacenza, Piazza Cavalli, 7, 29121 Piacenza PC, Italia', '9.692984499999966', '45.0526206', 'marco.castignoli@gmail.com', 1);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `rides`
--
ALTER TABLE `rides`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT per la tabella `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT per la tabella `requests`
--
ALTER TABLE `requests`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT per la tabella `rides`
--
ALTER TABLE `rides`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT per la tabella `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
