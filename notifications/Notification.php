<?php
require_once "../lib/config.php";


class Notification {

	public $user = NULL;
	public $title = NULL;
	public $description = NULL;
	public $link = NULL;
	public $fa = NULL;
	private $hash = NULL;
	
	public function send($from,$type){
		global $database;

		$this->hash = sha1($this->user.$from.$type);
		$count = $database->count("notifications",[
			"hash" => $this->hash,
		]);
		if ($count==0) {
			$database->insert("notifications", [
				"user" => $this->user,
				"hash" => $this->hash,
				"title" => $this->title,
				"description" => $this->description,
				"link" => $this->link,
				"fa" => $this->fa,
				"active" => 1,
			]);
		} else {
			$database->update("notifications",[
				"active" => 1,
			],[
				"hash" => $this->hash,
			]);
		}
	}

	public function deactivate($from,$type){
		global $database;
		$this->hash = sha1($this->user.$from.$type);
		$database->update("notifications",[
			"active" => 0,
		],[
			"hash" => $this->hash,
		]);
	}
}

?>