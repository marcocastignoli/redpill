<?php
require_once "../lib/config.php";
if (userLogged()) {
?>

	<div style="margin-top: 20px;" class="col-lg-8 col-lg-offset-2 text-center noselect">
		<h1 id="showNewEventButton" style="cursor:pointer;" state="0" onclick="showNewEvent(this)">Nuovo evento</h1>
		<i class="fa fa-angle-down fa-3x newEventShow_"></i>
		<hr class="light newEventShow">
		<b style="color: black;" id="error"></b> <br><br>
	</div>
<form class="newEventShow" id="newevent" enctype="multipart/form-data">
<?php
if ($_SESSION['User']->premium==1) {
?>
	<div class="col-lg-8 col-lg-offset-2 text-center">
		<p>Foto</p>
		<input id="newEventFile" onchange="setclickImage(this,'new')" style="display: none;" type="file" name="file">
		<img id="newEventImage" onclick="clickImage('new')" src="img/imgDefault.jpg" style="width: 100%; height: auto;">
	</div> 
	<div class="col-lg-8 col-lg-offset-2 text-center"><br></div>
<?php
}
?>
	<div class="col-lg-8 col-lg-offset-2 text-center">		
			<div class="col-md-4 text-center">
				<p>Info</p>
				<p><input placeholder="Titolo" class="input-sm" type="text" name="title"></p>
				<p><textarea style="border-radius: 10px; max-width: 100%;" placeholder="Descrizione" class="input-sm" type="password" name="description"></textarea></p>
				<p>Categoria</p>
				<p>
					<select class="input-sm" name="category">
						<?php
						$categories = new Categories();
						foreach ($categories->get() as $id => $category) {
							?>
							<option value="<?php echo $id; ?>"><?php echo $category['title']; ?></option>
							<?php
						}
						?>
					</select>
				</p>
			</div>
			<div class="col-md-4 text-center">
				<p>Dove</p>
				<p><input class="input-sm" placeholder="Nome locale" name="local" type="text"/></p>
				<p><input class="input-sm" name="place" type="text" id="us2-address"/></p>
				<p><div style="width: 100%; height: 200px;" id="us2" ></div></p>
				<input name="latitude" type="hidden" id="us2-lat"/>
				<input name="longitude" type="hidden" id="us2-lon"/>
				
				<!--<p><input placeholder="Luogo" class="input-sm" type="text" name="place"></p>-->
				<p>Quando</p>
				<p><input placeholder="Data" id="datepickerResult" class="input-sm" type="hidden" name="date"></p>
				<p><div style="background-color: white; color: black; width:220px; margin-left: calc(50% - 110px); border-radius: 10px;" id="datepicker"></div></p>
				<p><input id="timepicker" data-default-time="false" placeholder="Ore" class="input-sm" type="text" name="time"></p>
			</div>
			<div class="col-md-4 text-center">
				<p>Numero utenti massimo</p>
				<p><input placeholder="Vuoto per invitati ∞" class="input-sm" type="text" name="max"></p>
				<p>Tipo</p>
				<p>
					<select class="input-sm" name="type">
						<option value="0">Pubblico</option>
						<option value="1">Privato</option>
					</select>
				</p>
			</div>
	</div>
		
	<div style="margin-top: 20px;" class="col-md-12 text-center">
		<p><button id="publishBtn" class="btn btn-default btn-xl" type="submit">Pubblica</button></p>
	</div>
	
</form>

	
	<script> 	
	$("#newevent").submit(function() {
		$.ajax({
			type: "POST",
			url: "events/new.php",
			data: new FormData($(this)[0]),
			cache: false,
			contentType: false,
			processData: false,
			success: function(data){
				$("#publishBtn").html("Pubblica");
				var result = jQuery.parseJSON(data);
				if (result.newevent[0]==true) {
					$("#news").html('<div class="col-lg-8 col-lg-offset-2 text-center"><h1>Pubblicato</h1><p>L\'indirizzo per il tuo evento è:</p><p><b>http://red-pill.co/?evento='+result.newevent[1]+'</b></p><p>Condividi su: <br><a href="'+result.newevent[2]+'"><img height="25" width="25" src="img/fb_share.png"></a></p></div>');
					
					scrollTo("#News");
				} else {
					$("#error").html(result.newevent);
				}
			},beforeSend: function() {
				loading("#publishBtn");
			},
		});
		return false;
	});
	
	</script> 
<?php
} else {
?>
	<script>Reload();</script>
<?php
}
?>