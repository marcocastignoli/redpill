<?php
require_once "../lib/config.php";


$username=$_POST['username'];
$password=$_POST['password'];
$device=$_POST['device'];

$info = $database->select("users",
	"*", [
	"AND" => [
		"username" => $username,
		"password" => sha1($password),
	]
]);

$data["login"]=false;
$data["e"]=null;
if ($info!=null) {
	
	date_default_timezone_get();
	$exp = date("Y-m-d H:i:s", strtotime(date('Y-m-d H:i:s', time()) . " + 365 day"));

	$token_tmp = json_encode([
		"exp"=>$exp,
		"id"=>$info[0]['id'],
	]);
	$token=sha1($token_tmp);
	$database->insert("token", [
		"idUser"=>$info[0]['id'],
		"token"=>$token,
		"exp"=>$exp,
		"device"=>$device,
	]);
	$data['token']=$token;


	$user = new User();
	$user->id=$info[0]['id'];
	$user->username=$info[0]['username'];
	$user->name=$info[0]['name'];
	$user->surname=$info[0]['surname'];
	$user->place=$info[0]['place'];
	$user->latitude=$info[0]['latitude'];
	$user->longitude=$info[0]['longitude'];
	$user->premium=$info[0]['premium'];
	$user->email=$info[0]['email'];
	
	if (isset($_POST['idEvent']) && $_POST['idEvent']!="") {
		$data["e"]=$_POST['idEvent'];
	}
	
	$data['user']=$user;
	$data["login"]=true;
} else {
	$data["login"]=false;
}
echo json_encode($data);

?>