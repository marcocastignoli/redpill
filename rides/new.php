<?php
require_once "../lib/config.php";

if (userLogged()) {
	$phone=$_POST['phone'];
	$max=$_POST['max'];
	$place=$_POST['place'];
	$latitude=$_POST['latitude'];
	$longitude=$_POST['longitude'];
	$id=$_POST['id_event'];

	//errors
	$data["newride"]=null;

	if ($phone!="" && $max!="" && $latitude!="" && $longitude!="" && $place!="") {
		$database->insert("rides", [
			"id_event" => $id,
			"author" => $_SESSION['User']->id,
			"phone" => $phone,
			"max" => $max,
			"place" => $place,
			"latitude" => $latitude,
			"longitude" => $longitude,
		]);
		$data["newride"]=true;
	} else {
		$data["newride"]="Completa tutti i campi";
	}
	echo json_encode($data);
}
?>