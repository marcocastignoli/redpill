function loading(id){
	$(id).html("<center><i class='fa fa-cog fa-spin fa-4x'></i></center>");
}

function removeParameterFromUrl(url,parameterName){
    var reg = new RegExp('&?'+parameterName+'=([^&]$|[^&]*)','gi');     
    return url.replace(reg, ""); 
}

function scrollTo(page){
	$('html, body').animate({
        scrollTop: $(page).offset().top
    }, 1000);
}

function Login(idEvent){
	loading('#widgetContent');
	$('#widgetContent').load("widgets/login.php",{e:idEvent});
}

function Signin(idEvent){
	loading('#widgetContent');
	$('#widgetContent').load("widgets/signin.php",{e:idEvent},function(){
		$('#us1').locationpicker({
			location: {latitude: 0.0, longitude: 0.0},	
			radius: 1,
			enableAutocomplete: true,
			inputBinding: {
		        latitudeInput: $('#us1-lat'),
		        longitudeInput: $('#us1-lon'),
		        locationNameInput: $('#us1-address')
		    }
		});
	});
}

function EditUser(){
	$('html, body').animate({ scrollTop: 0 }, 0);
	loading('#widgetContent');
	$('#widgetContent').load("widgets/edituser.php");
}

function MyEvents(){
	$('html, body').animate({ scrollTop: 0 }, 0);
	loading('#widgetContent');
	$('#widgetContent').load("widgets/events.php");
}

function editEvent(idEvent){
	loading('#news');
	$('#news').load("news/editevent.php",{id:idEvent}, function() {
		scrollTo("#News");
		$('#datepicker').datepicker({
			format: "yyyy/mm/dd",
		}).on('changeDate', function(e){
			$('#datepickerResult').val(e.format('dd-mm-yyyy'))
		});

		$('#timepicker').timepicker({
			showMeridian: false,
			template: false,
            showInputs: false,
            minuteStep: 5
		});
		
	});
}

function deleteEvent(idEvent){
	$.ajax({
		type: "POST",
		url: "events/delete.php",
		data: {id:idEvent},
		success: function(data){
			var result = jQuery.parseJSON(data);
			if (result.deleteevent==true) {
				$("#news").html('<div class="col-lg-8 col-lg-offset-2 text-center"><h1>Eliminato</h1></div');
				MyEvents();
				$('html, body').scrollTop($("#News").offset().top);
			} else {
				$("#news").html('<div class="col-lg-8 col-lg-offset-2 text-center"><h1>'+result.deleteevent+'</h1></div');
			}
		},
		beforeSend: function() {
			loading("#news");
		},
	});
	return false;
}

function NewEvent(click){
	loading('#news');
	$('#news').load("news/newevent.php", function() {
		$('#datepicker').datepicker({
			format: "yyyy/mm/dd",
		}).on('changeDate', function(e){
			$('#datepickerResult').val(e.format('dd-mm-yyyy'))
		});

		$('#timepicker').timepicker({
			showMeridian: false,
			template: false,
            showInputs: false,
            minuteStep: 5
		});
		if ($('#us2').length) {
			$('#us2').locationpicker({
				location: {latitude: 0.0, longitude: 0.0},	
				radius: 1,
				enableAutocomplete: true,
				inputBinding: {
			        latitudeInput: $('#us2-lat'),
			        longitudeInput: $('#us2-lon'),
			        locationNameInput: $('#us2-address')
			    }
			});	
		}
		if (click) {
			$("#showNewEventButton").trigger("click");
		}
		
	});
}

function showNewEvent(obj){
	if ($(obj).attr('state')==0){
		$('.newEventShow').css('display','block');
		$('.newEventShow_').css('display','none');
		$(obj).attr('state',1);
		if ($('#us2').length) {
			$('#us2').locationpicker('autosize');
		}	
	} else {
		$('.newEventShow').css('display','none');
		$('.newEventShow_').css('display','block');
		$(obj).attr('state',0);
	}
}

function News(click){
	NewEvent(click);
	scrollTo("#News");
}

function MyRequests(){
	$('html, body').animate({ scrollTop: 0 }, 0);
	loading('#widgetContent');
	$('#widgetContent').load("widgets/myevents.php");
}

function sendRequest(id,obj,state){
	$.ajax({
		type: "POST",
		data: {e:id,s:state},
		url: "events/sendRequest.php",
		success: function(data){

			var result = jQuery.parseJSON(data);
			if(obj!=null){
				if (!result.sendrequest) {
					$(obj).attr("onclick","sendRequest("+id+",this,true);");
					$(obj).html("Invia richiesta!");
				} else {
					if (result.sendrequest=="pu") {
						$(obj).attr("onclick","sendRequest("+id+",this,false);");
						$(obj).html("Accettato!");
						
					} else if (result.sendrequest=="pr") {
						$(obj).attr("onclick","sendRequest("+id+",this,false);");
						$(obj).html("In attesa");		
					}
				}
			}
		},
		beforeSend: function() {
			if(obj!=null){
				$(obj).html("<center><i class='fa fa-cog fa-spin fa-2x'></i></center>");
			}
		},
	});
}

function acceptRequest(userID,eventID,obj){
	$.ajax({
		type: "POST",
		data: {u:userID,e:eventID},
		url: "events/acceptRequest.php",
		success: function(data){
			var result = jQuery.parseJSON(data);
			if (result.acceptRequest==true) {
				$(obj).parent().parent().remove();
			} else {
				$('#widgetContent').html('<div class="col-lg-8 col-lg-offset-2 text-center"><h1>'+result.acceptRequest+'</h1></div');
			}
		}
	});
}

function Search(){
	loading('#categories');
	$("#categories").load("search/categories.php");
	$("#events").html("");
	scrollTo("#search");
}

function Promo(){
	loading("#promo");
	$("#promo").load("promo/start.php");
	scrollTo("#portfolio");
}

function Event(idEvent){
	loading("#promo");
	$("#promo").load("promo/event.php",{id:idEvent});
	scrollTo("#portfolio");
}

function searchDate(day){
	$('#setDate .btn').removeClass("btnDateSelected");
	$('#setDate .btn').addClass("btnDate");
	$('#setDate .btn').eq(day).removeClass("btnDate").addClass("btnDateSelected");
	$("#timeSelector").val(day);
	if (day==0) {
		$("#defaultHour").text("Adesso");
	} else {
		$("#defaultHour").text("Giornata");
	}
}

function searchTime(hour,obj){
	$('#setTime .btn').removeClass("btnDateSelected");
	$('#setTime .btn').addClass("btnDate");
	$(obj).removeClass("btnDate").addClass("btnDateSelected");
	$("#hourSelector").val(hour);
}

function selectorDate(day){
	$('#timeSelector').val(day);
	if (day==0) {
		$("#defaultHour1").text("Adesso");
	} else {
		$("#defaultHour1").text("Giornata");
	}
}

function Category(cat,obj,page){
	var distance=$("#distanceSlider").val();
	var when=$("#timeSelector").val();
	var hour=$("#timeSelector").val();
	var tags=[];
	$(".tags").each(function( index ) {
		tags.push($(this).attr("value"));
	});
	if (obj!=null) {
		var category = $(obj);
		$("#categories").html(category);
	}
	
	if(!navigator.geolocation){
		$("#events").load("search/events.php",{c:cat,p:page,d:distance,w:when,t:tags,h:hour});
		return;
	}

	function success(p){
		$("#events").load("search/events.php",{c:cat,p:page,l:p.coords,d:distance,w:when,t:tags,h:hour});
	};

	function error(err){
		if (err.code==3){
			$("#categories").append("<center><p class='text-muted'><b>Non riesco a trovarti!<br>Controlla le impostazioni di localizzazione del tuo dispositivo</b></p></center>");
		}
		$("#events").load("search/events.php",{c:cat,p:page,d:distance,w:when,t:tags,h:hour});
	};

	var options={
		timeout: 20000,
		maximumAge:6000,
		enableHighAccuracy:true,
	};

	loading("#events");
	$("#events").append("<br><center><p>Se il caricamento dura troppo,<br> controlla le tue impostazioni di localizzazione!</p></center>");
	navigator.geolocation.getCurrentPosition(success,error,options);
}

function Logout(){
	$.ajax({
		type: "POST",
		url: "users/logout.php",
		success: function(data){
			clearInterval(refresh);
			Reload();
		}
	});
}

function Reload(){
	loading('#widgetContent');
	$('#widgetContent').load("widgets/start.php");
	$('#menu').load("topbar/menu.php",function(){
		$('.navbar-collapse a').not(".notifications").click(function (e) {
			if ($(window).width()<767) {
				$('.navbar-collapse').collapse('toggle');
			}
		});
		$("#notif").load("topbar/notification.php");
	});
	loading("#promo");
	$("#promo").load("promo/start.php");
	loading("#news");
	$('#news').load("news/start.php");
	loading("#categories");
	$("#categories").load("search/categories.php");
	loading("#footer");
	$("#footer").load("footer/footer.php");

	$("#events").html("");
	$('html, body').animate({ scrollTop: 0 }, 0);
}

function notiAction(action,obj,eventID){
	$.ajax({
		type: "POST",
		data: {id:eventID},
		url: "notifications/delete.php",
		success: function(data){
			var result = jQuery.parseJSON(data);
			if (result.deleteNotification==true) {
				
			}
		}
	});
	if (action!=null) {
		eval(action);
	}
}

function updateNotification(){

	$.ajax({
		type: "POST",
		url: "notifications/update.php",
		success: function(data){
			var result = jQuery.parseJSON(data);
			if (result.update!=null) {
				$("#notif").load("topbar/notification.php");
				var actual = result.update;
				if (actual!=0) {
					$("#menuNoti").css("display","inline-block");
					$("#menuNoti").html(actual);
				} else {
					$("#menuNoti").css("display","none");
				}
			}
		}
	});
}

function clearNotification(){
	$.ajax({
		type: "POST",
		url: "notifications/clear.php",
		success: function(data){
			var result = jQuery.parseJSON(data);
			if (result.cl==true) {
				$("#notif").load("topbar/notification.php");
				$("#menuNoti").css("display","none");
			}
		}
	});
}

function showPartecipants(obj,target){
	if ($(obj).attr('state')==0){
		$('#participants'+target).css('display','block');
		$('#showPartecipants'+target).removeClass('fa-angle-down');
		$('#showPartecipants'+target).addClass('fa-angle-up');
		$(obj).attr('state',1);
		$('#participants'+target).load("events/partecipants.php",{id:target});

	} else {
		$('#participants'+target).css('display','none');
		$('#showPartecipants'+target).removeClass('fa-angle-up');
		$('#showPartecipants'+target).addClass('fa-angle-down');
		$(obj).attr('state',0);
	}
}

function showRides(obj,target,requestFrom){
	if ($(obj).attr('state')==0){
		$('#rides'+requestFrom+target).css('display','block');
		$('#showRides'+requestFrom+target).removeClass('fa-angle-down');
		$('#showRides'+requestFrom+target).addClass('fa-angle-up');
		$(obj).attr('state',1);
		if ($('#us'+requestFrom+target).length) {
			$('#us'+requestFrom+target).locationpicker('autosize');
		}
		//rides
		$.ajax({
			data: {id:target},
			type: "POST",
			url: "rides/get.php",
			success: function(data){
				var result = jQuery.parseJSON(data);
				if (result.getride!=null) {
					var locations = result.getride;
					var map = new google.maps.Map(document.getElementById('map'+requestFrom+target), {
						zoom: 10,
						center: new google.maps.LatLng(locations[0].eLat, locations[0].eLon),
						mapTypeId: google.maps.MapTypeId.ROADMAP
					});

					var infowindow = new google.maps.InfoWindow();

					var marker, i;

					for (i = 0; i < locations.length; i++) {  
						marker = new google.maps.Marker({
							position: new google.maps.LatLng(locations[i].latitude, locations[i].longitude),
							map: map,
							icon: "img/car.png",
						});
						google.maps.event.addListener(marker, 'click', (function(marker, i) {
							return function() {
								infowindow.setContent("<div style='color: black !important;'>"+locations[i].phone+"<br>"+locations[i].name+" "+locations[i].surname+"</div>");
								infowindow.open(map, marker);
								}
						})(marker, i));
					}
					var evento = new google.maps.Marker({
						position: new google.maps.LatLng(locations[0].eLat, locations[0].eLon),
						map: map,
						icon: "img/redpill.png",
					});
				}
			}
		});

	} else {
		$('#rides'+requestFrom+target).css('display','none');
		$('#showRides'+requestFrom+target).removeClass('fa-angle-up');
		$('#showRides'+requestFrom+target).addClass('fa-angle-down');
		$(obj).attr('state',0);
	}
}

function showNewRide(obj,target,requestFrom){
	$(obj).next().css( "display", "inline");
	if ($('#us'+requestFrom+target).length) {
		$('#us'+requestFrom+target).locationpicker('autosize');
	}
	$(obj).remove();
}

function clickImage(type){
	var elem = document.getElementById(type+'EventFile');
	if (elem && document.createEvent) {
		var evt = document.createEvent("MouseEvents");
		evt.initEvent("click", false, false);
		elem.dispatchEvent(evt);
	}
}

function setclickImage(obj,type){
	var elem = $('#'+type+'EventImage');
	var file = $(obj)[0].files[0];
	if (FileReader && file) {
		var fr = new FileReader();
		fr.onload = function () {
			elem.attr("src", fr.result);
		}
		fr.readAsDataURL(file);
	} else {
		console.log("Non supportato.");
	}

}

function showAllDescription(obj){
	if ($(obj).attr("state")==0) {
		$(obj).removeClass("descriptionClosed");
		$(obj).attr("state","1");
	}else{
		$(obj).addClass("descriptionClosed");
		$(obj).attr("state","0");
	}
	
}