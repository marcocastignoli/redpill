<?php
require_once "../lib/config.php";
require_once "../rides/UI.php";

if (userLogged()) {

@$category = $_POST['c'];
$page=$_POST['p'];
@$location=$_POST['l'];
@$distance=$_POST['d'];
@$when=$_POST['w'];
@$hour=$_POST['h'];
@$tags=$_POST['t'];

$tagsText="1=1";
if (isset($tags)) {
	$tagsText = "1=0";
	foreach ($tags as $tag) {
		$tagsText.=" OR title LIKE ".$database->quote("%".$tag."%");
	}
}

if (!isset($distance)) {
	$distance=10;
}

$latitude=null;
$longitude=null;

if (isset($location)) {
	$latitude=$location['latitude'];
	$longitude=$location['longitude'];
	?>
	<center><p>Sto usando la tua localizzazione <i class="fa fa-location-arrow"></i></p></center>
	<?php
} else {
	$latitude=$_SESSION['User']->latitude;
	$longitude=$_SESSION['User']->longitude;
}

date_default_timezone_get();
$date = null;
if ($when!=0) {
	$date = date('Y-m-d', time());
	$date=date('Y-m-d G:i', strtotime($date . ' + '.$when.' day'));
	if ($hour!=0) {
		$date=date('Y-m-d G:i', strtotime($date . ' + '.$hour.' hour'));
	}
} else {
	if ($hour==0) {
		$date = date('Y-m-d G:i', time());
	} else {
		$date = date('Y-m-d', time());
		$date=date('Y-m-d G:i', strtotime($date . ' + '.$hour.' hour'));
	}
	
}

$sessionID= isset($_SESSION['User']->id) ? $_SESSION['User']->id : 0;



if ($category==null) {
	$query = "	SELECT id,author,title,description,place,local,latitude,longitude,date,max,category,type,photo,col1,col2, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_SESSION['User']->id." AND id_event = events.id) as status
		FROM events
		WHERE (".$tagsText.")
		HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
		ORDER BY date ASC
		LIMIT 8 OFFSET $page
	";
	$data = $database->query($query)->fetchAll();
} else {
	$data = $database->query("	SELECT id,author,title,description,place,local,latitude,longitude,date,max,category,type,photo,col1,col2, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_SESSION['User']->id." AND id_event = events.id) as status
								FROM events 
								WHERE category = $category AND (".$tagsText.")
								HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
								ORDER BY date ASC
								LIMIT 8 OFFSET $page
	")->fetchAll();
}
?>
<center>
<p><a class="btn btn-primary btn-xl" onclick="Search();">Back</a></div></p>
</center>

<?php

$fixColumn=0;
foreach ($data as $event) {

?>
	<div class="col-sx-12 col-sm-6 col-md-4 col-lg-3 text-center searchContainer"><div class="event" style="padding: 0px !important; background-color: <?php echo $event['col1']; ?> !important; ">
		
		<?php
		$oldLocale = setlocale(LC_TIME, 'it_IT');
		$datet = utf8_encode( strftime("%a %d %b<br>%H:%M", strtotime($event['date'])) );
		?>
		<div style="padding: 10px !important; font-family: 'Open Sans', 'Helvetica Neue', Arial, sans-serif !important; color: <?php echo $event['col2']; ?> !important;">
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: left !important; color: white;">
			<h4 style="color: <?php echo $event['col2']; ?> !important;">
		<?php 
			if ($event['type']==0 && $sessionID!=0) { 
		?>
			<a style="color: <?php echo $event['col2']; ?> !important;" href="http://maps.apple.com/?q=<?php echo $event['latitude']; ?>,<?php echo $event['longitude']; ?>"><?php echo $event['local']; ?>  <i class="fa fa-location-arrow"></i></a> 
		<?php 
			} else if ($event['type']==1 && $sessionID!=0) { 
				if ($event['status']!=null) {
					if ($event['status'] == 1) {
						?>
						<a style="color: <?php echo $event['col2']; ?> !important;" href="http://maps.apple.com/?q=<?php echo $event['latitude']; ?>,<?php echo $event['longitude']; ?>"><?php echo $event['local']; ?>  <i class="fa fa-location-arrow"></i></a> 
						<?php
					} else {
						echo $event['local'];
					}
				} else {
					echo $event['local'];
				}
			}
		?> 
			</h4>
		</div>
			<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="text-align: right !important; color: <?php echo $event['col2']; ?> !important;"><?php echo $datet; ?></div>
		</div>
		<br><br>
		<div class="image_evt">
			<?php
			if ($event['photo']!="") {
			?>
				<img src="<?php echo $event['photo']; ?>" width="100%" height="auto">
			<?php
			} else {
			?>
				<img src="img/rp_event.jpg" width="100%" height="auto">
			<?php
			}
			if ($event['author']!=$sessionID) {
				if($event['status'] != null){
					if ($event['status'] == 0) {	
			?>
						<div onclick="sendRequest(<?php echo $event['id']; ?>,this,false);" class="btnrequest" style="cursor: pointer;">In attesa</div>
			<?php
					} else if ($event['status'] == 1) {
			?>
						<div onclick="sendRequest(<?php echo $event['id']; ?>,this,false);"  class="btnrequest" style="cursor: pointer;">Accettato!</div>
			<?php
					}
				} else {
			?>
					<div onclick="sendRequest(<?php echo $event['id']; ?>,this,true);"  class="btnrequest" style="cursor: pointer;">Invia richiesta!</div>
			<?php
				}
			} else {
				?>
					<div onclick="editEvent(<?php echo $event['id']; ?>);" class="btnrequest" style="cursor: pointer;">Modifica evento</div>
				<?php
			}
			?>
			<div class="btncounter" style="text-align: right;">
				<?php
				if ($event['max']!=0) {
				?>
					<?php echo $event['requestsCount']; ?>/<?php echo $event['max']; ?> <i class="fa fa-users"></i>
				<?php
				} else {
				?>
					<?php echo $event['requestsCount']; ?> <i class="fa fa-users"></i>
				<?php
				}
				?>
			</div>
		</div>
		<h3 style="color: <?php echo $event['col2']; ?> !important;"><?php echo $event['title']; ?></h3>
		<p state="0" onclick="showAllDescription(this);" class="text-faded descriptionClosed" style="color: <?php echo $event['col2']; ?> !important;"><small><?php echo $event['description']; ?></small></p>
	
	<p></p>
	<?php
	//passaggi
	rides($event,"search",$event['col2']);
	?>
	<br>
	</div></div>

	<?php
	if ($fixColumn==1) {
	?>
		<div class="clearfix visible-sm"></div>
	<?php
	} else if ($fixColumn==2) {
	?>
		<div class="clearfix visible-md"></div>
	<?php
	} else if ($fixColumn==3) {
	?>
		<div class="clearfix visible-lg"></div>
	<?php
	}
	$fixColumn++;
}



if ($category==null) {
	$count = $database->query("	SELECT COUNT(*)
								FROM events 
								WHERE (events.max=0 OR events.max > (select count(*) from requests where status = 1 AND id_event = events.id))  AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
								AND (".$tagsText.")
	")->fetchAll();
} else {
	$count = $database->query("	SELECT COUNT(*)
								FROM events 
								WHERE category = '$category'
								and (events.max=0 OR events.max > (select count(*) from requests where status = 1 AND id_event = events.id))  AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
								AND (".$tagsText.")
	")->fetchAll();
}
$count=$count[0][0];

?>

	<nav style="margin-top: 20px !important;" class="col-xs-12">
	<center>
	<ul class="pagination">
		
<?php
$count=$count/8;
if ($count<1) {
	if($count==0){
		$count=0;
	} else {
		$count=1;	
	}
}
function is_decimal( $val ){
    return is_numeric( $val ) && floor( $val ) != $val;
}

if (is_decimal($count)) {
	$count=(int)$count+1;
}
for ($i=0; $i<$count; $i++) { 
?>
		<li><a <?php if($page==$i*8){echo 'class="activeMenu"';} ?> onclick="Category(<?php echo $category ?>,null,<?php echo $i*8; ?>);$('body').scrollTop($('#search').offset().top);" style="color:#f05f40;  border-color: #f05f40;" href="javascript:;" ><?php echo $i+1; ?></a></li>
<?php
}
?>
		
	</ul>
	</center>
	</nav>
<?php
}
?>
<script>
	$(function(){
		$('#us5').locationpicker({
			radius: 1,
			enableAutocomplete: true,
			inputBinding: {
		        latitudeInput: $('#us5-lat'),
		        longitudeInput: $('#us5-lon'),
		        locationNameInput: $('#us5-address')
		    }
		});

	});
</script>
<!--<center><p style="margin-top: 20px;" class="col-xs-12"><a class="btn btn-primary btn-xl" onclick="Category(<?php //echo $category ?>,null,<?php //echo $page+8; ?>)">Load other events...</a></p></center>-->