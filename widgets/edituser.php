<?php
require_once "../lib/config.php";
require_once "../wolfood/UI.php";

if (userLogged()) {

	if (!@$_SESSION['FBID']) {
?>
	
	<form id="edituser">
		<b style="color: #f05f40;" id="error"></b><br><br>
		<p><input value=<?php echo '"'.$_SESSION['User']->email.'"'; ?> placeholder="Email" class="input-sm" type="text" name="email"></p>
		<p><input value=<?php echo '"'.$_SESSION['User']->username.'"'; ?> placeholder="Username" class="input-sm" type="text" name="username"></p>
		<p><input placeholder="Password" class="input-sm" type="password" name="password"></p>
		<p>
			
			<input value=<?php echo '"'.$_SESSION['User']->name.'"'; ?>  placeholder="Name" class="input-sm" type="text" name="name">
			<p class="visible-sx"> </p>
			<input value=<?php echo '"'.$_SESSION['User']->surname.'"'; ?>  placeholder="Surname" class="input-sm" type="text" name="surname">
		</p>
		<p><input value=<?php echo '"'.$_SESSION['User']->place.'"'; ?> class="input-sm" name="place" type="text" id="us1-address"/></p>
		<p><div style="width: 100%; height: 200px;" id="us1" ></div></p>
		<input name="latitude" type="hidden" id="us1-lat"/>
		<input name="longitude" type="hidden" id="us1-lon"/>
		<p>
			<label>Seleziona per eliminare il tuo account:</label>
			<br>
			<input type="checkbox" name="method" value="1"></p>
		<p>
			<input value="Update" class="btn btn-primary btn-xl" type="submit" />
		</p>
		<p><a style="background-color: #41559c" class="btn btn-primary btn-xl" onclick="window.location='fb/fbconfig.php';" href="javascript:;">Sincronizza con facebook</a> <?php //connectWolfood(); ?></p>
	</form>
	
	<script> 
	$("#edituser").submit(function() {
		$.ajax({
			type: "POST",
			url: "users/edit.php",
			data: $("#edituser").serialize(),
			success: function(data){
				var result = jQuery.parseJSON(data);
				if (result.edituser==true) {
					$("#error").css("color","#f05f40");
					$("#error").html("Change saved");
				} else {
					if (result.edituser==404) {
				        Reload();
					}
					$("#error").css("color","#f05f40");
					$("#error").html(result.edituser);
				}
			}
		});
		return false;
	});
	$(function(){
		$('#us1').locationpicker({
			location: {latitude: <?php echo '"'.$_SESSION['User']->latitude.'"'; ?>, longitude: <?php echo '"'.$_SESSION['User']->longitude.'"'; ?>},	
			radius: 1,
			enableAutocomplete: true,
			inputBinding: {
		        latitudeInput: $('#us1-lat'),
		        longitudeInput: $('#us1-lon'),
		        locationNameInput: $('#us1-address')
		    }
		});
	});
	</script> 
<?php
	} else {
?>
	<h1>Cambia le tue informazioni su facebook</h1>
	<p>
<?php
	//connectWolfood();
	?>
	</p>
	<?php
	}
} else {
?>
	<script>Reload();</script>
<?php
}
?>