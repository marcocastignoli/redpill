<?php
require_once "../lib/config.php";

$id = $_POST['id'];

if (userLogged()) {
	$database->delete("notifications", [
		"AND"=>[
			"id" => $id,
			"user" => $_SESSION['User']->id,
		],
	]);
	$data["deleteNotification"]=true;
} else {
	$data["deleteNotification"]=false;
}
echo json_encode($data);
?>