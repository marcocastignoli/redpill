<?php
require_once "../lib/config.php";
function Day($n){
    switch ((date("N")-1+$n)%7+1) {
        case 1:
            return "Lunedì";
            break;
        case 2:
            return "Martedì";
            break;
        case 3:
            return "Mercoledì";
            break;
        case 4:
            return "Giovedì";
            break;
        case 5:
            return "Venerdì";
            break;
        case 6:
            return "Sabato";
            break;
        case 7:
            return "Domenica";
            break;
        default:
            return "Error";
            break;
    }
}
if (userLogged()) {
?>
<center>
    <p id="distanceViewer">Cercherai eventi entro: <b>10 km</b> di distanza.</p>
    <p><input id="distanceSlider" type="text" data-slider-id="RC" data-slider-min="1" data-slider-max="50" data-slider-step="1" data-slider-value="10"/></p>
    <p>Imposta una data e un momento della giornata</p>
    <p>
        <div id="setDate" class="btn-group hidden-xs" role="group" aria-label="">
            <button onclick="searchDate(0);" type="button" class="btn btn-default btnDateSelected">Oggi</button>
            <button onclick="searchDate(1);" type="button" class="btn btn-default btnDate">Domani</button>
            <button onclick="searchDate(2);" type="button" class="btn btn-default btnDate"><?php echo Day(2); ?></button>
            <button onclick="searchDate(3);" type="button" class="btn btn-default btnDate"><?php echo Day(3); ?></button>
            <button onclick="searchDate(4);" type="button" class="btn btn-default btnDate"><?php echo Day(4); ?></button>
            <button onclick="searchDate(5);" type="button" class="btn btn-default btnDate"><?php echo Day(5); ?></button>
            <button onclick="searchDate(6);" type="button" class="btn btn-default btnDate"><?php echo Day(6); ?></button>
            <button onclick="searchDate(7);" type="button" class="btn btn-default btnDate"><?php echo Day(7); ?></button>
        </div>
        <select onchange="selectorDate($(this).val());" class="input-sm visible-xs">
            <option value="0">Oggi</option>
            <option value="1">Domani</option>
            <option value="2"><?php echo Day(2); ?></option>
            <option value="3"><?php echo Day(3); ?></option>
            <option value="4"><?php echo Day(4); ?></option>
            <option value="5"><?php echo Day(5); ?></option>
            <option value="6"><?php echo Day(6); ?></option>
            <option value="7"><?php echo Day(7); ?></option>
        </select>
        <input type="hidden" id="timeSelector" value="0">
        <p> </p>
        <div id="setTime" class="btn-group hidden-xs" role="group" aria-label="">
            <button id="defaultHour" onclick="searchTime(0,this);" type="button" class="btn btn-default btnDateSelected">Adesso</button>
            <button onclick="searchTime(8,this);" type="button" class="btn btn-default btnDate">Mattina</button>
            <button onclick="searchTime(12,this);" type="button" class="btn btn-default btnDate">Pomeriggio</button>
            <button onclick="searchTime(16,this);" type="button" class="btn btn-default btnDate">Sera</button>
            <button onclick="searchTime(20,this);" type="button" class="btn btn-default btnDate">Notte</button>
        </div>
        <select onchange="$('#hourSelector').val($(this).val());" class="input-sm visible-xs">
            <option id="defaultHour1" value="0">Adesso</option>
            <option value="8">Mattina</option>
            <option value="12">Pomeriggio</option>
            <option value="16">Sera</option>
            <option value="20">Notte</option>
        </select>
        <input type="hidden" id="hourSelector" value="0">
    </p>
    <p>Imposta dei filtri (premi invio per confermare)</p>
    <p>
        <div id="filtriContainer">
            <div style="border-radius: 300px 0px 0px 300px;" class="filtri">
                <i class="fa fa-tags"></i>
            </div></div><input id="filtriInput" type="text" class="input-sm">
    </p>
    
</center>
<div id="categories_container" class="multi-slider owl-carousel clearfix">
    <figure id="0" onclick="Category(null,this,0)" class="item"> 
        <div style="padding: 10px; text-align: center; cursor: pointer;" class="service-box">
            <i class="fa fa-4x fa-globe wow bounceIn text-primary"></i>
            <h3>Tutti</h3>
            <p class="text-muted">Se sei proprio senza idee, qui vedi tutti gli eventi!</p>
        </div>
    </figure>

    <?php
    $categories = new Categories();
    foreach ($categories->get() as $id => $category) {
    ?>
    <figure id="<?php echo $id+1; ?>" onclick="Category(<?php echo $id; ?>,this,0)" class="item"> 
        <div style="padding: 10px; text-align: center; cursor: pointer;" class="service-box">
            <i class="fa fa-4x <?php echo $category['icon']; ?> wow bounceIn text-primary"></i>
            <h3><?php echo $category['title']; ?></h3>
            <p class="text-muted"><?php echo $category['description']; ?></p>
        </div>
    </figure>
    <?php
    }
    ?>
</div>

<script>
    $(document).ready(function() {
        $("#categories_container").owlCarousel({
            lazyLoad : true,
            slideSpeed : 300,
            autoPlay: true,
            pagination: false,
            items: 3,
            navigation:true,
            itemsTablet: [600,2], //2 items between 600 and 0
            itemsMobile : false
        });
        $("#distanceSlider").slider({
            formatter: function(value) {
                return value+' km';
            }
        });
        $("#distanceSlider").on("slide", function(slideEvt) {
            $("#distanceViewer").html("Cercherai eventi entro: <b>"+slideEvt.value+" km</b> di distanza.");
        });
        $("#filtriInput").keypress(function(e) {
            if(e.which == 13 || e.which == 32) {
                var filtro = $("#filtriInput").val();
                if (filtro!="" && filtro!=" ") {
                    $("#filtriContainer").append("<div value="+filtro+" class='filtri tags'>"+filtro+" <i onclick='$(this).parent().remove();' class='fa fa-times'></i></div>");
                }
                $("#filtriInput").val("");
            }
        });
     });
</script>

<?php
} else {
?>
    <div class="col-lg-8 col-lg-offset-2 text-center">
        <p>Non sai cosa fare? Si sta avvicinando una serata social network e noia? Cerca eventi vicino a te con redpill, li trovi già divisi in categorie, così ti è più facile scegliere cosa vuoi vuoi fare!</p>
        <a onclick="Login();scrollTo('body')" href="javascript:;" class="btn btn-primary btn-xl wow tada">Login</a>
    </div>
<?php
}
?>