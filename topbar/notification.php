<?php
require_once "../lib/config.php";
if (userLogged()) {
    $data = $database->select("notifications","*",[
        "AND"=>[
            "active" => 1,
            "user"=>$_SESSION['User']->id,
        ],
        "ORDER"=>"datetime DESC",
    ]);
?>
<a class="notifications" href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> <div id="menuNoti1" style="background-color: #f05f40; color:white !important; text-align:center; width: 20px; height: 20px; border-radius: 100%; <?php if($data!=null) {?> display: inline-block; <?php } else { ?> display: none; <?php } ?>"><?php echo count($data); ?></div> Notifiche<b class="caret"></b></a>
<ul class="dropdown-menu">

    
    <?php
    if($data) {
        ?>
        <li>
            <a href="javascript:;" onclick="clearNotification();" style="text-align: center; width:100% !important;">
                <i class="fa fa-times"></i>
            </a>
        </li>
        <?php
        foreach ($data as $key => $notification) {
        ?>
            <li style="min-width: 300px; border-top: 1px solid #ccc;">
                <a href="javascript:;" onclick="notiAction('<?php echo $notification['link']; ?>',this,<?php echo $notification['id']; ?>)">
                    <i class="fa <?php echo $notification['fa']; ?>"></i> <b><?php echo $notification['title']; ?></b><br>
                    <?php echo $notification['description']; ?>
                </a>
            </li>
        <?php
        }
    } else {
        ?>
        <li style="min-width: 300px;">
            <a style="text-align:center; cursor:pointer;">Non hai notifiche</a>
        </li>
        <?php
    }
    ?>
</ul>

<?php
}
?>