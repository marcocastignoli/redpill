<?php
//passaggi

function rides($event,$requestFrom){
	$color=$event['col2'];
	global $database;
	$rides = $database->select("rides","*",[
		"id_event" => $event['id'],
	]);
	
	?>

		<div state="0" style="cursor: pointer; color: <?php echo $color; ?> !important;" onclick="showRides(this,<?php echo $event['id']; ?>,'<?php echo $requestFrom; ?>')"><small>Passaggi</small> <i id="showRides<?php echo $requestFrom; ?><?php echo $event['id']; ?>" class=" fa fa-angle-down"></i></div>
	    <br>
	    <div style="display: none; background-color: rgba(0,0,0,0.3); margin-bottom: 10px !important; padding: 10px !important; " id="rides<?php echo $requestFrom; ?><?php echo $event['id']; ?>">
			<?php
			$ifEdit=0;
			if (count($rides)>0) {
			?>
	    	<br>
	    	<div id="map<?php echo $requestFrom; ?><?php echo $event['id']; ?>" style="width: 100%; height: 200px;"></div>
	    	<br>
	    	<?php
			$latMyRide=null;
			$lonMyRide=null;
			
				foreach ($rides as $ride) {
					if ($ride['author']==$_SESSION['User']->id) {
						$ifEdit++;
						$latMyRide=$ride['latitude'];
						$lonMyRide=$ride['longitude'];

						?>
						<form id="editride">
							<p><b style="color: #f05f40;" id="errorRide"></b></p>
							<p><input value="<?php echo $ride['phone']; ?>" placeholder="Telefono" class="input-sm" type="text" name="phone"></p>
							<p><input value="<?php echo $ride['max']; ?>" placeholder="Numero posti" class="input-sm" type="text" name="max"></p>
							<p><input value="<?php echo $ride['place']; ?>" class="input-sm" name="place" type="text" id="us<?php echo $requestFrom; ?><?php echo $event['id']; ?>-address"/></p>
							<p><div style="width: 100%; height: 200px;" id="us<?php echo $requestFrom; ?><?php echo $event['id']; ?>" ></div></p>
							<input name="latitude" type="hidden" id="us<?php echo $requestFrom; ?><?php echo $event['id']; ?>-lat"/>
							<input name="longitude" type="hidden" id="us<?php echo $requestFrom; ?><?php echo $event['id']; ?>-lon"/>
							<input name="id_event" type="hidden" value="<?php echo $ride['id_event']; ?>" />
							<input name="id" type="hidden" value="<?php echo $ride['id']; ?>" />
							<p>
								<label style="color: white;">Seleziona per eliminare il tuo passaggio:</label>
								<br>
								<input type="checkbox" name="method" value="1"></p>
							<p>
								<input value="Aggiorna" class="btn btn-default btn-xl" type="submit" />
							</p>
							
						</form>
						<script> 
						$("#editride").submit(function() {
							$.ajax({
								type: "POST",
								url: "rides/edit.php",
								data: $("#editride").serialize(),
								success: function(data){
									var result = jQuery.parseJSON(data);
									if (result.editride==true) {
										$("#errorRide").css("color","white");
										$("#errorRide").html("Change saved");
									} else {
										if (result.editride==404) {
											$("#editride").remove();
										}else {
											$("#errorRide").css("color","white");
											$("#errorRide").html(result.editride);
										}
										
									}
								}
							});
							return false;
						});
						$(function(){
							$('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>').locationpicker({
								location: {latitude: <?php echo $latMyRide; ?>, longitude: <?php echo $lonMyRide; ?>},	
								radius: 1,
								enableAutocomplete: true,
								inputBinding: {
							        latitudeInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lat'),
							        longitudeInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lon'),
							        locationNameInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-address')
							    }
							});
						});
						</script> 
						<?php
					}
				}
			}
				if ($ifEdit==0) {
					?>
	    			<small onclick="showNewRide(this,<?php echo $event["id"]; ?>,'<?php echo $requestFrom; ?>')" style="color: rgba(255, 255, 255, 0.701961); cursor: pointer;">Aggiungi un passaggio <i class=" fa fa-plus"></i></small>
	    			<form style="display:none;" id="newride">
						<p><b style="color: #f05f40;" id="errorNewRide"></b></p>
						<p><input value="" placeholder="Telefono" class="input-sm" type="text" name="phone"></p>
						<p><input value="" placeholder="Numero posti" class="input-sm" type="text" name="max"></p>
						<p><input value="" class="input-sm" name="place" type="text" id="us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-address"/></p>
						<p><div style="width: 100%; height: 200px;" id="us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>" ></div></p>
						<input name="latitude" type="hidden" id="us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lat"/>
						<input name="longitude" type="hidden" id="us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lon"/>
						<input name="id_event" type="hidden" value="<?php echo $event['id']; ?>" />
						<input name="id" type="hidden" value="" />
						<p>
							<input value="Aggiungi" class="btn btn-default btn-xl" type="submit" />
						</p>
					</form>
					<script>
						$("#newride").submit(function() {
							$.ajax({
								type: "POST",
								url: "rides/new.php",
								data: $("#newride").serialize(),
								success: function(data){
									var result = jQuery.parseJSON(data);
									if (result.newride==true) {
										$("#newride").html("<center>Passaggio aggiunto</center>");
									} else {
										$("#errorNewRide").css("color","white");
										$("#errorNewRide").html(result.newride);
									}
								}
							});							
							return false;
						});
						$(function(){
							$('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>').locationpicker({
									location: {latitude: <?php echo $event['latitude']; ?>, longitude: <?php echo $event['longitude']; ?>},	
									radius: 1,
									enableAutocomplete: true,
									inputBinding: {
								        latitudeInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lat'),
								        longitudeInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-lon'),
								        locationNameInput: $('#us<?php echo $requestFrom; ?><?php echo $event["id"]; ?>-address')
								    }
							});
						});
					</script>
				<?php
				}
			?>
	    </div>
	    
	<?php
	?>
<?php
}

?>