<?php
require_once "../lib/config.php";
if (userLogged()) {
?>
    <li>
        <a onclick="Reload()" class="page-scroll" href="javascript:;">Home</a>
    </li>
    <li>
        <a onclick="EditUser()" class="page-scroll" href="javascript:;"><?php echo $_SESSION['User']->name; ?></a>
    </li>
    <li id="notif" class="dropdown" onclick="updateNotification();">
    
    </li>
    <li>
        <a class="page-scroll" onclick="Promo();" href="javascript:;">Eventi in primo piano</a>
    </li>
    <li>
        <a onclick="MyEvents()" class="page-scroll" href="javascript:;">I miei eventi</a>
    </li>
    <li>
        <a onclick="MyRequests()" class="page-scroll" href="javascript:;">Richieste</a>
    </li>
    <li>
        <a class="page-scroll" onclick="Logout();" href="javascript:;">Logout</a>
    </li>
<?php 
} else {
?>
	<li>
        <a class="page-scroll" onclick="Signin();" href="javascript:;">Registrati</a>
    </li>
    <li>
        <a class="page-scroll" onclick="Login();" href="javascript:;">Login</a>
    </li>
<?php
}
?>