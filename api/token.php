<?php
require_once "../lib/config.php";

function userVerified($id,$token){
	global $database;
	$check = $database->count("token","*",[
		"AND"=>[
			"token"=>$token,
			"idUser"=>$id,
			"exp[>]"=>date('Y-m-d H:i:s', time()),
		],
	]);
	return $check>0;
}

?>