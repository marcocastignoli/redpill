<?php
require_once "../lib/config.php";

$username=$_POST['username'];
$password=$_POST['password'];
$name=$_POST['name'];
$surname=$_POST['surname'];
$place=$_POST['place'];
$longitude=$_POST['longitude'];
$latitude=$_POST['latitude'];
$email=$_POST['email'];
$type=@$_POST['method'];

//errors
$data["edituser"]=null;

if ($username!="" && $password!="" && $name!="" && $surname!="" && $place!="" && $email!="") {
	//controllo se username esiste
	$count = $database->count("users", [
		"username" => $username
	]);

	if ($count==0 || $username==$_SESSION['User']->username) {
		$info = $database->count("users", [
			"AND" => [
				"username" => $username,
				"password" => sha1($password),
			]
		]);
		if ($info!=null) {
			if ($type!=1) {
				//update user
				$database->update("users", [
					"username" => $username,
					"password" => sha1($password),
					"name" => $name,
					"surname" => $surname,
					"place" => $place,
					"longitude" => $longitude,
					"latitude" => $latitude,
					"email" => $email,
				],
				[
					"id" => $_SESSION['User']->id,
				]);
				$user = new User();
				$user->id=$_SESSION['User']->id;
				$user->username=$username;
				$user->password=$password;
				$user->name=$name;
				$user->surname=$surname;
				$user->place=$place;
				$user->longitude=$longitude;
				$user->latitude=$latitude;
				$user->email=$email;
				$_SESSION["User"] = $user;
				$data["edituser"]=true;
			} else {
				//delete user
				$database->delete("users", [
					"id" => $_SESSION['User']->id,
				]);
				$database->delete("events", [
					"author" => $_SESSION['User']->id,
				]);
				$database->delete("requests", [
					"id_user" => $_SESSION['User']->id,
				]);
				session_destroy();
				$data["edituser"]=404;
			}
			
		} else {
			$data["edituser"]="Devi prima immettere la tua password";
		}

	} else {
		$data["edituser"]="Questo username esiste già";
	}
}
echo json_encode($data);
?>