<?php
require_once "../lib/config.php";
?>
<br>
<?php

$idEvent = $_POST['id'];

$check = $database->count("events",[
	"AND"=>[
		"author"=>$_SESSION['User']->id,
		"id"=>$idEvent,
	],
]);

if ($check!=0) {
	$data = $database->select("users",[
		"[>]requests" => ["id" => "id_user"],
	],"*",[
		"requests.id_event"=>$idEvent,
	]);
	foreach ($data as $partecipant) {
		?>
		<small><?php echo $partecipant['name']." ".$partecipant['surname']; ?></small><br>
		<?php
	}
}
?>