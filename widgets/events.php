<?php
require_once "../lib/config.php";
require_once "../share/facebook_share.php";
if (userLogged()) {

$data = $database->select("events",[
	"id",
	"title",
	"description",
    "max",
    "date",
    "photo",
],[
	"author"=>$_SESSION['User']->id,
    "ORDER" => "date DESC",
]);

if (count($data)!=0) {
    ?>
    <div id="myevents_container" class="multi-slider owl-carousel clearfix">
    <?php

    foreach ($data as $event) {
    ?>

        <figure class="item"> 
            <div style="padding: 10px; text-align: center; text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" class="service-box">
                <h3 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;"><?php echo $event['title']; ?></h3>
                <br>
                <?php
                $count = $database->count("requests",[
                    "AND"=>[
                        "id_event"=>$event['id'],
                        "status"=>1,
                    ],
                ]);
                if ($event['max']!=0) {
                    ?>
                    <center>
                        <div class="progress" style="max-width: 250px;">
                            <div class="progress-bar" role="progressbar" aria-valuenow=<?php echo '"'.$count.'"'; ?> aria-valuemin="0" aria-valuemax=<?php echo '"'.$event['max'].'"'; ?> style="width: <?php echo $count*100/$event['max'];  ?>%; min-width:15%; background-color: #222;">
                                <?php echo $count; ?>/<?php echo $event['max']; ?>
                            </div>
                        </div>
                    </center>
                    <?php
                }else{
                ?>
                    <div style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;">Parteciperanno <b><?php echo $count; ?></b> persone.</div><br>
                <?php
                }
                ?>
                    <div  state="0" style="cursor: pointer;" onclick="showPartecipants(this,<?php echo $event['id']; ?>)"><small style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;">Partecipanti</small> <i id="showPartecipants<?php echo $event['id']; ?>" class=" fa fa-angle-down"></i></div>
                    <div style="max-height: 200px; display: none; overflow-y: scroll; text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" id="participants<?php echo $event['id']; ?>">
                        
                    </div>
                    <br>
                    <?php
                        $sharer = new Share($event['id'],$event['title'],$event['description'],@$event['photo']);
                    ?>
                    <!--<a href="<?php echo $sharer->url(); ?>"><img height="25" width="25" src="img/fb_share.png"></a><br><br>-->
                <a <?php echo 'onclick="editEvent('.$event['id'].')"'; ?> href="javascript:;" class="btn btn-primary btn-xl wow tada">Modifica</a>
            </div>
        </figure>

    <?php
    }
    ?>
    </div>
    <script>
        $(document).ready(function() {
            $("#myevents_container").owlCarousel({
                lazyLoad : true,
                slideSpeed : 300,
                autoPlay: false,
                pagination: false,
                items: 3,
                navigation:true,
                itemsTablet: [600,2], //2 items between 600 and 0
                itemsMobile : false
            });
         });
    </script>
    <?php
} else {
    ?>
        <h1>Non hai eventi</h1>
    <?php
}
}
?>