<?php
header('Access-Control-Allow-Origin: *');
require_once "../lib/config.php";

function Day($n){
    switch ((date("N")-1+$n)%7+1) {
        case 1:
            return "Lunedì";
            break;
        case 2:
            return "Martedì";
            break;
        case 3:
            return "Mercoledì";
            break;
        case 4:
            return "Giovedì";
            break;
        case 5:
            return "Venerdì";
            break;
        case 6:
            return "Sabato";
            break;
        case 7:
            return "Domenica";
            break;
        default:
            return "Error";
            break;
    }
}
$data['days']=[
	Day(2),
	Day(3),
	Day(4),
	Day(5),
	Day(6),
	Day(7),
];
$categories=new Categories();
$data['categories']=$categories->get();

echo json_encode($data);

?>