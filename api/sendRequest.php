<?php
require_once "../lib/config.php";
require_once "../notifications/Notification.php";
require_once "token.php";

$id_event = $_POST['e'];
$idUser = $_POST['id'];
$token = $_POST['token'];



$data["sendrequest"]=null;

if (userVerified($idUser,$token)) {
	$info = $database->get("events", ["max","id","author","type"], [
		"id" => $id_event,
	]);
	$user=$database->get("users", "*", [
		"id" => $_SESSION['User']->id,
	]);

	$check=$database->count("requests", [
		"AND"=>[
			"id_event" => $id_event,
			"id_user" => $_SESSION['User']->id,
		],
	]);

	$noti = new Notification();
	$noti->user=$info['author'];

	if ($_POST['s']=="true") {
		//controllo se l'evento è pubblico o privato
		if ($info['max']==0 && $info['type']==0) {
			$database->insert("requests", [
				"id_event" => $id_event,
				"id_user" => $_SESSION['User']->id,
				"status" => "1",
			]);
			$noti->title="Un nuovo partecipante!";
			$noti->description=$user['name']." ".$user['surname']." parteciperà al tuo evento.";
			$noti->link="Event(".$id_event.")";
			$noti->fa="";
			$data["sendrequest"]="pu";
		} else {
			$database->insert("requests", [
				"id_event" => $id_event,
				"id_user" => $_SESSION['User']->id,
				"status" => "0"
			]);
			$noti->title="Hai una richiesta";
			$noti->description=$user['name']." ".$user['surname']." ti ha inviato una richiesta.";
			$noti->link="MyRequests()";
			$noti->fa="";
			$data["sendrequest"]="pr";
		}
		if ($check==0) {
			$noti->send($_SESSION['User']->id, "request".$id_event);	
		}
	} else {
		$database->delete("requests", [
			"AND" => [
				"id_event" => $id_event,
				"id_user" => $_SESSION['User']->id,
			]
		]);
		$noti->deactivate($_SESSION['User']->id, "request".$id_event);
		$data["sendrequest"]=false;
	}
} else {
	$data['error']="token";
}
echo json_encode($data);
?>