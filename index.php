<?php
    require_once "lib/config.php";
?>
<!DOCTYPE html>
<html lang="en" xmlns:fb="http://www.facebook.com/2008/fbml">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>redpill</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">

    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
    <link rel="stylesheet" href="css/animate.min.css" type="text/css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/creative.css" type="text/css">


    <link href="owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link id="bsdp-css" href="lib/datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link id="bsdp-css" href="lib/timepicker/bootstrap-timepicker.css" rel="stylesheet">

    <link href="lib/bootstrap-slider.css" rel="stylesheet">
    <script src="sections.js"></script>



<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-title" content="iOS Web App">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="format-detection" content="telephone=no">
<link rel="apple-touch-icon" href="ico.png">
</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <?php
                    if (userLogged()) {
                        $count = $database->count("notifications",[
                            "AND"=>[
                                "active" => 1,
                                "user"=>$_SESSION['User']->id,
                            ],
                        ]);
                    }
                    ?>
                    <div id="menuNoti" style="background-color: #f05f40; color:white !important; text-align:center; width: 20px; height: 20px; border-radius: 100%; position: absolute; top:-5px; right:-5px; display: <?php if (userLogged()) { if($count!=0) {?> inline-block; <?php } else { ?> none; <?php } } else { ?> none; <?php } ?>"><?php if (userLogged()) { echo $count; } ?></div>
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a style="padding: 0px;" onclick="Reload();" class="navbar-brand page-scroll" href="#page-top"><img class="redpill" src="logo.png"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul id="menu" class="nav navbar-nav navbar-right">


                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div id="widgetContent" class="header-content-inner">

            </div>
        </div>
    </header>

    <section class="bg-dark" id="portfolio">
        <div class="container-fluid">
            <div id="promo" class="row no-gutter">

            </div>
        </div>
    </section>

    <section id="News" class="bg-primary" id="about">
        <div class="container">
            <div id="news" class="row">

            </div>
        </div>
    </section>

    <section id="search">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Ricerca eventi</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div id="categories" class="container">

        </div>

        <div id="events" class="container">

        </div>
    </section>

    <div id="footer">

    </div>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/jquery.fittext.js"></script>
    <script src="js/wow.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="js/creative.js"></script>
    <!-- DATEPICKER -->
    <script src="lib/datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="lib/timepicker/bootstrap-timepicker.js"></script>


    <script src="owl-carousel/owl.carousel.min.js"></script>
        <!-- location view -->

    <script type="text/javascript" src='http://maps.google.com/maps/api/js?sensor=false&libraries=places&key=AIzaSyCsndVj927OWkRc23GmyQWP7gsVgDIVCjQ'></script>
    <script src="lib/locationpicker.jquery.js"></script>

    <script src="lib/bootstrap-slider.js"></script>
    <script>Reload();  </script>
    <?php
        if (userLogged()) {
            if (isset($_GET['send'])) {
    ?>
            <script>
                $(window).load(function(){
                    Event(<?php echo (int)$_GET['send']; ?>,null,true);
                });
            </script>
            <?php
                }
    ?>
        <script>var refresh=setInterval(updateNotification,2000);</script>
    <?php
        }
        if (isset($_GET['evento'])) {
    ?>
    <script>
        $(window).load(function(){
            Event(<?php echo (int)$_GET['evento']; ?>,null,true);
        });
    </script>
    <?php
        }

    ?>

</body>

</html>
