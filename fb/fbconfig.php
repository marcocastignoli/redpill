<?php
$event=@$_GET['e'];
function domain(){
  if ($_SERVER['SERVER_PORT']=="8080") {
    return $_SERVER['SERVER_NAME'].':'.$_SERVER['SERVER_PORT'].'/projects/games';
  } else {
    return substr($_SERVER['SERVER_NAME'], 4);
  }
}
//require_once "../lib/config.php";
require_once 'autoload.php';
require 'functions.php';  
use Facebook\FacebookSession;
use Facebook\FacebookRedirectLoginHelper;
use Facebook\FacebookRequest;
use Facebook\FacebookResponse;
use Facebook\FacebookSDKException;
use Facebook\FacebookRequestException;
use Facebook\FacebookAuthorizationException;
use Facebook\GraphObject;
use Facebook\GraphLocation;
use Facebook\Entities\AccessToken;
use Facebook\HttpClients\FacebookCurlHttpClient;
use Facebook\HttpClients\FacebookHttpable;

// init app with app id and secret
FacebookSession::setDefaultApplication( '1604352923184730','aa8b8c09291062ddf72111cc5dc87e99' );
// login helper with redirect_uri
    if (isset($event) && $event!="") {
      $helper = new FacebookRedirectLoginHelper('http://'.domain().'/fb/fbconfig.php?e='.$event );
    } else {
      $helper = new FacebookRedirectLoginHelper('http://'.domain().'/fb/fbconfig.php' );
    }
try {
  $session = $helper->getSessionFromRedirect();
} catch( FacebookRequestException $ex ) {
  // When Facebook returns an error
} catch( Exception $ex ) {
  // When validation fails or other local issues
}
// see if we have a session
if ( isset( $session ) ) {
  // graph api request for user data
  $request = new FacebookRequest( $session, 'GET', '/me' );
  $response = $request->execute();
  // get response
  $graphObject = $response->getGraphObject();
     	$fbid = $graphObject->getProperty('id');              // To Get Facebook ID
 	    $fbname = $graphObject->getProperty('first_name'); // To Get Facebook full name
	    $fbsurname = $graphObject->getProperty('last_name');    // To Get Facebook email ID

            $objloc=$graphObject->getProperty('location');
            if ( isset($objloc) ) {
              $fbplace = $graphObject->getProperty('location')->getProperty('name');
            $locationID=$graphObject->getProperty('location')->getProperty('id');
            
            
            $app_token_url = "https://graph.facebook.com/oauth/access_token?"
            . "client_id=" . '1604352923184730'
            . "&client_secret=" . 'aa8b8c09291062ddf72111cc5dc87e99' 
            . "&grant_type=client_credentials";

            $response = file_get_contents($app_token_url);
            $params = null;
            parse_str($response, $params);

            $contents = file_get_contents("https://graph.facebook.com/".$locationID."/feed?access_token=".$params['access_token']);
            $contents = json_decode($contents, true);
            }
            
      
      
      $email=$graphObject->getProperty('email');

    
      
      /*    */
     
  /* ---- Session Variables -----*/
      $sendNotification="";
      $_SESSION["FBID"] = true;
      $id=checkuser($fbid,$fbname,$fbsurname,@$fbplace,@$contents['location'],$event,$email);
      if (isset($event) && $event!="") {
        $sendNotification="?send=".(int)$event;
      }
    /* ---- header location after session ----*/
  //header("Location: ../index.php");

  echo "
  <script>
      window.location='http://".domain()."/".$sendNotification."';
  </script>";
  
} else {
  $loginUrl = $helper->getLoginUrl(array('user_location','email'));
 //header("Location: ".$loginUrl);
 echo "<script>
        window.location='".$loginUrl."';
    </script>";
}


?>