<?php
require_once "../lib/config.php";

$phone=$_POST['phone'];
$max=$_POST['max'];
$place=$_POST['place'];
$latitude=$_POST['latitude'];
$longitude=$_POST['longitude'];
$id=$_POST['id'];
$id_event=$_POST['id_event'];
$method=@$_POST['method'];

//errors
$data["editride"]=null;

$authorCheck=$database->count("rides",[
	"AND"=>[
		"id"=>$id,
		"author"=>$_SESSION['User']->id
	]
]);

if ($authorCheck==1) {
	if ($method!=1) {
		if ($phone!="" && $max!="" && $latitude!="" && $longitude!="" && $place!="") {
			$check = $database->update("rides", [
				"id_event" => $id_event,
				"author" => $_SESSION['User']->id,
				"phone" => $phone,
				"max" => $max,
				"place" => $place,
				"latitude" => $latitude,
				"longitude" => $longitude,
			],[
				"id" => $id,
			]);
			if ($check!="") {
				$data["editride"]=true;
			} else {
				$data["editride"]="Riprova più tardi...";
			}
		} else {
			$data["editride"]="Completa tutti i campi";
		}
	} else {
		$database->delete("rides", [
			"id" => $id,
		]);
		$data["editride"]=404;
	}
} else {
	$data["editride"]="Non puoi modificare i passaggi degli altri!";
}



echo json_encode($data);
?>