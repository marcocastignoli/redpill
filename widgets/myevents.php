<?php
require_once "../lib/config.php";

if (userLogged()) {
	
		
		$events = $database->select("events",[
			"id",
			"title",
		],[
			"author"=>$_SESSION['User']->id,
			"ORDER" => "id DESC",
		]);
		if (count($events)!=0) {
		
				$idEvent=[];
				foreach ($events as $e){
					$idEvent[]=$e['id'];
				}
				$titleEvent=[];
				foreach ($events as $e){
					$titleEvent[$e['id']]=$e['title'];
				}
				$data = $database->select("users",[
						"[>]requests" => ["id" => "id_user"],
					],[
						"users.id",
						"users.name",
						"users.surname",
						"users.fb",
						"requests.id_event",
					],[
						"AND" => [
							"requests.id_event" => $idEvent,
							"requests.status" => 0,
						],
						
					]);
				if ($data!=false) {
		?>
				<div id="myevents_container" class="multi-slider owl-carousel clearfix">
		<?php
				
				foreach ($data as $utente) {
			?>
				
				    <figure class="item"> 
				        <div style="padding: 10px; text-align: center;" class="service-box">
				            <?php
				            	if ($utente['fb']!=0) {
				            ?>
				            		<!--<a target="_blank" href="https://www.facebook.com/app_scoped_user_id/<?php echo $utente['fb']; ?>"><img height="70" width="70" class="wow bounceIn text-primary img-circle" src="//graph.facebook.com/<?php echo $utente['fb']; ?>/picture"></a>-->
				            <?php
								}
				            ?>
				            
				            <h3 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;"><?php echo $utente['name']." ".$utente['surname']; ?></h3>
				            <div style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;"><?php echo $titleEvent[$utente['id_event']]; ?></div><br><br>
				            <a onclick="<?php echo 'acceptRequest('.$utente['id'].','.$utente['id_event'].',this)'; ?>" href="javascript:;" class="btn btn-primary btn-xl wow tada">Accetta</a>
				        </div>
				    </figure>
				
			<?php
				}
				?>
				</div>
				<script>
				    $(document).ready(function() {
				        $("#myevents_container").owlCarousel({
				            lazyLoad : true,
				            slideSpeed : 300,
				            autoPlay: false,
				            pagination: false,
				            items: 3,
				            navigation:true,
				            itemsTablet: [600,2], //2 items between 600 and 0
				            itemsMobile : false
				        });
				     });
				</script>
				<?php
			} else{
				?>
					<h1>Non hai richieste</h1>
				<?php
			}
		} else {
			?>
				<h1>Non hai eventi</h1>
			<?php
		}
}
?>