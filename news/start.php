<?php
require_once "../lib/config.php";
if (!userLogged()) {
?>
    <div class="col-lg-8 col-lg-offset-2 text-center">
        <h2 class="section-heading">Crea eventi!</h2>
        <hr class="light">
        <p class="text-faded">Con redpill creare momenti per divertirsi insieme diventa più facile. Le possibilità sono infinite: magari ti mancano due persone per il calcetto, hai voglia di studiare in compagnia, oppure vuoi organizzare una partita a risiko.<br>Redpill ti offre la possibilità di staccarti dagli schermi digitali, per dedicarti al divertimento vero, ti aspettiamo.</p>
        <a onclick="Login();scrollTo('body')" href="javascript:;" class="btn btn-default btn-xl wow tada">Login</a>
    </div>
<?php
} else {
?>
	<script>NewEvent();</script>
<?php
}
?>