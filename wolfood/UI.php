<?php
require_once "../lib/config.php";
//require_once "connection.php";

function connectWolfood(){
	if (userLogged()) {
		global $database;
		//prendo i dati relativi al negozio, utilizzando $_SESSION['User']->id
		/*
			$data = mssql_query(" 	SELECT * FROM users
									INNER JOIN redpill ON users.id=redpill.id_wf
									WHERE  redpill.id_dp=".$database->quote($_SESSION['User']->id));
			$row = mssql_fetch_array($data);
			error_log(print_r($row,true));
		*/
		if (isset($row)) {
			if ($row['active']==1) { 
			?>
				<p>Sei collegato a wolfood, al negozio <b><?php /* echo $negozio['name'] */ ?></b>.</p>
			<?php
			} else {
				?>
				<p>
					Accedi a Wolfood e accetta la connessione, altrimenti:<br><br>
					<button onclick="undoWolfoodPin();" class="btn btn-danger btn-xl">ANNULLA</button>
				</p>
				<script>
					function undoWolfoodPin(){
						$.ajax({
							type: "POST",
							url: "wolfood/undo.php",
							success: function(data){
								if (data==true) {
									$("#wolfood").html(<?php //connectWolfood(); ?>);
								}
							}
						});
						return false;
					}
				</script>
			<?php
			}
		} else {
			?>
				<div id="wolfood">
					<form id="connectWolfood">
						<input value="<?php echo $_SESSION['User']->id; ?>" type="hidden" name="id_dp">
						<input class="input-sm" style="width: 150px !important; border-radius: 20px 20px 0px 0px; margin: 0px !important;" value="" placeholder="Codice negozio" type="text" name="id_wf"><br>
						<button class="btn btn-primary btn-sm" style="width: 150px !important; border-radius: 0px 0px 20px 20px; margin: 0px !important;" type="submit"><br><img height="50" width="auto" src="img/wf.png"><br><br>Connetti</button>
					</form>
					<script>
						$("#connectWolfood").submit(function() {
							$.ajax({
								type: "POST",
								url: "wolfood/ask.php",
								data: $("#connectWolfood").serialize(),
								success: function(data){
									if (data) {
										$("#wolfood").html(data);
									}
								}
							});
							return false;
						});
					</script>
				</div>
			<?php
		}

	}
}

?>