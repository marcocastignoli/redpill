<?php
require_once "../lib/config.php";
require_once "../images/Images.php";
require_once "../share/facebook_share.php";

function validateDate($date, $format = 'Y-m-d H:i:s'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

if (userLogged()) {
	$title=$_POST['title'];
	$description=$_POST['description'];
	$place=$_POST['place'];
	$date=$_POST['date'];
	$latitude=$_POST['latitude'];
	$longitude=$_POST['longitude'];
	$time=$_POST['time'];
	$max=$_POST['max'];
	$category=$_POST['category'];
	$type=$_POST['type'];
	$local=$_POST['local'];
	
	//errors
	$data["newevent"]=null;

	if ($title!="" && $description!="" && $place!="" && $date!="" && $time!="" && $category!="" && $type!="" && $local!="") {
		
		$datetime = new DateTime($date);
		$max=(int)$max;
		if ($max>=0) {
			if(validateDate($datetime->format('Y-m-d'),'Y-m-d')){
				$time=explode( ':', $time );
				if ((is_numeric($time[0]) && (int)$time[0]>=0 && (int)$time[0]<=23) && ( is_numeric($time[1]) && (int)$time[1]>=0 && (int)$time[1]<=59)) {
					$datetime->setTime($time[0],$time[1],"00");
					$categories = new Categories();

					if ($category>=0 && $category <$categories->cou() ) {
						if ($type==0 || $type ==1) {
							if ($max!=1) {
								$id = $database->insert("events", [
									"author" => $_SESSION['User']->id,
									"title" => $title,
									"description" => $description,
									"place" => $place,
									"date" => $datetime->format('Y-m-d H:i:s'),
									"max" => $max,
									"category" => $category,
									"type" => $type,
									"latitude" => $latitude,
									"longitude" => $longitude,
									"local" => $local,
									"col1" => "#f05f40",
									"col2" => "black",
								]);
								$photo = "";
								if (isset($_FILES['file'])) {
									$photo = addPhotoEvent($_FILES['file'],$id);
									if (!$photo) {
										$data["editevent"]="Problemi con il caricamento della foto.";
									}
								}
								$database->insert("requests", [
									"id_event" => $id,
									"id_user" => $_SESSION['User']->id,
									"status" => "1",
								]);
								$sharer = new Share($id,$title,$description,@$photo);
								$data["newevent"]=[true,$id,$sharer->url()];
							} else {
								$data["newevent"]="Non puoi creare un evento al quale partecipi soltanto te";
							}
						} else {
							$data["newevent"]="Controlla il tipo";
						}
					} else {
						$data["newevent"]="Controlla la categoria";
					}
				} else {
					$data["newevent"]="Controlla l'orario";
				}
			} else {
				$data["newevent"]="Controlla la data";
			}
		} else {
			$data["newevent"]="Il numero di persone non può essere negativo.";
		}
		
	} else {
		$data["newevent"]="Fill in all fields, please.";
	}
	echo json_encode($data);
}
?>