<?php
require_once "../lib/config.php";

if (userLogged()) {
	$id=$_POST['id'];
	$data["deleteevent"]=null;
	if (isAuthor($id)) {	
		$database->delete("events", [
			"id"=>$id,
		]);
		$database->delete("requests", [
			"id_event"=>$id,
		]);
		$data["deleteevent"]=true;
	} else {
		$data["deleteevent"]="O c'è un problema nel sito o nella tua testa.";
	}
	echo json_encode($data);
}
?>