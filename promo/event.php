<?php
require_once "../lib/config.php";
require_once "../rides/UI.php";

$id=$database->quote($_POST['id']);
$event = $database->query("	SELECT id,author,title,description,place,local,latitude,longitude,date,max,category,type,photo,col1,col2, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount
							FROM events 
							WHERE id=$id
")->fetchAll();
$event=$event[0];

if ($event!="") {

?>


<center>
	<p><a class="btn btn-primary btn-xl" onclick="Promo();">Back</a></div></p>
</center>
<div class="col-md-4 hidden-sm"></div>
<div class="col-md-4 col-sm-12 text-center" style="padding: 10px;">
	<div class="event" style="padding: 0px !important; background-color: <?php echo $event['col1']; ?> !important; ">
		


		<h3 style="color: white;" class="section-heading"><?php echo $event['title']; ?></h3>
		<hr class="light">
		<p class="text-faded"><?php echo $event['description']; ?></p>
	<?php
	//se l'evento è pubblico mostra tutti i dati
	//controllo la richiesta dell'utente
	$check = null;
	if (isset($_SESSION['User']->id)) {
		$check = $database->select("requests", "status", [
			"AND" => [
				"id_event" => $event['id'],
				"id_user" => $_SESSION['User']->id,
			]
		]);
	}
	

	$sessionID= isset($_SESSION['User']->id) ? $_SESSION['User']->id : 0;
	

	if ($event['type']==0 /*&& $sessionID!=0*/) {
	?>
		<p class="text-faded">
			<small><?php echo $event['place']; ?></small><br>
			<small><?php echo date('d/m/Y H:i', strtotime($event['date'])); ?></small>
		</p>
		<p class="text-faded"></p>
	<?php
	//se l'evento è privato avvisa l'utente
	} else if ($event['type']==1 /*&& $sessionID!=0 */) {
		
		if ($check!=null) {
			if ($check[0] == 0) {
		?>
				<p class="text-faded"><small>Non puoi visualizzare i dettagli, l'organizzatore deve prima accettare la tua richiesta</small></p>
		<?php
			} else if ($check[0] == 1) {
		?>
				<p class="text-faded">
					<small><a style="color: rgba(255, 255, 255, 0.7);" href="http://maps.apple.com/?q=<?php echo $event['latitude']; ?>,<?php echo $event['longitude']; ?>"><?php echo $event['place']; ?></a></small><br>
					<small><?php echo date('d/m/Y H:i', strtotime($event['date'])); ?></small>
				</p>
				<p class="text-faded"></p>
		<?php
			}
		} else {
		?>
				<p class="text-faded"><small>Non puoi visualizzare i dettagli, l'evento è privato.</small></p>
		<?php
		}
	}
	if ($event['max']!=0) {
	?>
		<div class="progress">
			<div class="progress-bar" role="progressbar" aria-valuenow=<?php echo '"'.$event['requestsCount'].'"'; ?> aria-valuemin="0" aria-valuemax=<?php echo '"'.$event['max'].'"'; ?> style="width: <?php echo $event['requestsCount']*100/$event['max'];  ?>%; min-width:15%; background-color: #222;">
				<?php echo $event['requestsCount']; ?>/<?php echo $event['max']; ?>
			</div>
		</div>
	<?php
	} 
	if (userLogged()) {
		
		//passaggi UI
		rides($event,"promo");
	    ?>
	    <br><br>
	    <?php
		if ($event['author']!=$sessionID) {
			if($check != null){
				if ($check[0] == 0) {	
		?>
					<a onclick="sendRequest(<?php echo $event['id']; ?>,this,false);" href="javascript:;" class="btn btn-default btn-xl">In attesa...<br><small>(Premi per annullare)</small></a>
		<?php
				} else if ($check[0] == 1) {
		?>
					<a onclick="sendRequest(<?php echo $event['id']; ?>,this,false);" href="javascript:;" class="btn btn-default btn-xl">Accettato!<br><small>(Premi per annullare)</small></a>
		<?php
				}
			} else {
		?>
				<a onclick="sendRequest(<?php echo $event['id']; ?>,this,true);" href="javascript:;" class="btn btn-default btn-xl">Invia richiesta!</a>
		<?php
			}
		} else {
			?>
				<a onclick="editEvent(<?php echo $event['id']; ?>);" href="javascript:;" class="btn btn-default btn-xl">Modifica evento</a>
			<?php
		}
	} else {
		?>
		<hr>
		<p>Per accettare l'evento:</p>
		<a onclick="Login(<?php echo $event['id']; ?>);scrollTo('body')" href="javascript:;" class="btn btn-default btn-xl">Effettua il login<br><small>(anche con facebook)</small></a>  
		<br><br>
		<a onclick="Signin(<?php echo $event['id']; ?>);scrollTo('body')" href="javascript:;" class="btn btn-default btn-xl">oppure registrati</a>
		<?php
	}
	?>
	<br><br>
	</div></div>
	<div class="col-md-4 hidden-sm"></div>
	<?php
} else {
	?>
	<center><h1>L'EVENTO NON ESISTE OPPURE È STATO ELIMINATO</h1></center>
<?php
	}
?>
