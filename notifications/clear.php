<?php
require_once "../lib/config.php";
$data["cl"]=false;
if (userLogged()) {
	$database->delete("notifications", [
		"AND"=>[
			"user" => $_SESSION['User']->id,
		],
	]);
	$data["cl"]=true;
}
echo json_encode($data);
?>