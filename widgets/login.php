<?php
require_once "../lib/config.php";
if (!userLogged()) {
	$idEvent=@$_POST['e'];
?>
	<h1 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;">Login</h1>
	<hr>
	<form id="login">
		<b style="color: #f05f40;" id="error"></b><br><br>
		<p><input placeholder="Username" class="input-sm" type="text" name="username"></p>
		<p><input placeholder="Password" class="input-sm" type="password" name="password"></p>
		<input type="hidden" name="idEvent" value="<?php echo $idEvent; ?>">
		<p><button class="btn btn-primary btn-xl" type="submit">Login</button></p>
		<!--<p><a style="background-color: #41559c" class="btn btn-primary btn-xl" onclick="window.location='fb/fbconfig.php?e=<?php echo $idEvent; ?>';" href="javascript:;">Login with Facebook</a></div></p>-->
	</form>
	<script> 
	$("#login").submit(function() {
		$.ajax({
			type: "POST",
			url: "users/login.php",
			data: $("#login").serialize(),
			success: function(data){
				var result = jQuery.parseJSON(data);
				if (result.login==true) {
					Reload();
					if (result.e!=null) {
						Event(result.e,null,true);
					}
					refresh = setInterval(updateNotification,2000);
				} else {
					$("#error").html("Authentication error");
				}
			}
		});
		return false;
	}); 
	</script> 
<?php
} else {
?>
	<script>Reload();</script>
<?php
}
?>