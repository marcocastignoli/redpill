<?php
require_once "../lib/config.php";
$data["update"]=null;
if (userLogged()) {
	$count=$database->count("notifications", [
		"AND"=>[
            "active" => 1,
            "user"=>$_SESSION['User']->id,
        ],
	]);
	$data["update"]=$count;
} else {
	$data["update"]=null;
}
echo json_encode($data);
?>