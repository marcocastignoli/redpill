<?php
require_once "../lib/config.php";
require_once "../rides/UI.php";
require_once "token.php";

$token=$_POST['token'];

$data=null;
if (userVerified($_POST['user']['id'],$token)) {
	
	@$category = $_POST['c'];
	$page=$_POST['p'];
	@$location=$_POST['l'];
	@$distance=$_POST['d'];
	@$when=$_POST['w'];
	@$hour=$_POST['h'];
	@$tags=$_POST['t'];

	$tagsText="1=1";
	if (isset($tags)) {
		$tagsText = "1=0";
		foreach ($tags as $tag) {
			$tagsText.=" OR title LIKE ".$database->quote("%".$tag."%");
		}
	}

	if (!isset($distance)) {
		$distance=10;
	}

	$latitude=null;
	$longitude=null;

	if (isset($location)) {
		$latitude=$location['latitude'];
		$longitude=$location['longitude'];
		$data['gps']=true;
	} else {
		$latitude=$_POST['user']["latitude"];
		$longitude=$_POST['user']["longitude"];
		$data['gps']=false;
	}

	date_default_timezone_get();
	$date = null;
	if ($when!=0) {
		$date = date('Y-m-d', time());
		$date=date('Y-m-d G:i', strtotime($date . ' + '.$when.' day'));
		if ($hour!=0) {
			$date=date('Y-m-d G:i', strtotime($date . ' + '.$hour.' hour'));
		}
	} else {
		if ($hour==0) {
			$date = date('Y-m-d G:i', time());
		} else {
			$date = date('Y-m-d', time());
			$date=date('Y-m-d G:i', strtotime($date . ' + '.$hour.' hour'));
		}
		
	}

	if ($category==null) {
		$data['events'] = $database->query("	SELECT id,author,title,description,place,local,latitude,longitude,date,max,category,type,photo,col1,col2, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_POST['user']['id']." AND id_event = events.id) as status
									FROM events
									WHERE (".$tagsText.")
									HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
									ORDER BY date ASC
									LIMIT 8 OFFSET $page
		")->fetchAll();
	} else {
		$data['events'] = $database->query("	SELECT id,author,title,description,place,local,latitude,longitude,date,max,category,type,photo,col1,col2, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_POST['user']['id']." AND id_event = events.id) as status
									FROM events 
									WHERE category = $category AND (".$tagsText.")
									HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
									ORDER BY date ASC
									LIMIT 8 OFFSET $page
		")->fetchAll();
	}

	//sistemo array

	foreach ($data['events'] as $k => $event) {
		$oldLocale = setlocale(LC_TIME, 'it_IT');
		$data['events'][$k]['date'] = utf8_encode( strftime("%a %d %b<br>%H:%M", strtotime($event['date'])) );
		foreach ($event as $key => $value) {

		    if (is_int($key)) {
		        unset($data['events'][$k][$key]);
		    }
		}	
	}
} else {
	$data['error']="token";
}

echo json_encode($data);

?>