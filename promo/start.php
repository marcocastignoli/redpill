<?php
require_once "../lib/config.php";
date_default_timezone_get();
$date = date('Y-m-d H:i:s', time());

$data = $database->select("events",[
        "[>]users"=>["author"=>"id"],
    ],
    ["events.id","events.date(date)","events.author","events.photo","users.premium"],[
        "AND"=>[
            "users.premium"=>1,
            "date[>]" => $date,
            "events.photo[!]" => "",
        ]
    ]
);

?>
<div id="promo_container" class="multi-slider owl-carousel clearfix">
<?php

foreach ($data as $event) {
?>
    
        <div class="item">
            <a onclick="Event(<?php echo $event['id'] ?>);" href="javascript:;" class="portfolio-box">
                <img src="<?php echo $event['photo'] ?>" class="img-responsive" alt="">
            </a>
        </div>
    
<?php
}

$numElem=0;
switch (count($data)) {
    case 1:
        $numElem = 1;
        break;
    case 2:
        $numElem = 2;
        break;
    default:
        $numElem = 3;
        break;
}

?>
</div>
<script>
    $(document).ready(function() {
        $("#promo_container").owlCarousel({
            lazyLoad : true,
            slideSpeed : 300,
            autoPlay: true,
            pagination: false,
            items: <?php echo $numElem; ?>,
            navigation:true,
            itemsTablet : [1200,2],
            itemsMobile : [800,1],
        });
     });
</script>