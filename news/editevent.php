<?php
require_once "../lib/config.php";
if (userLogged()) {
$data=$database->select("events", "*", [
	"id" => $_POST['id'],
]);
$date_time=explode(' ', $data[0]['date']);
$time=str_replace("-","/",$date_time[0]);
?>
<form id="editevent" enctype="multipart/form-data">
<?php
/*
	tasto elimina immagine
	poi pensa a nuova che è easy
*/
if ($_SESSION['User']->premium==1) {
?>
	<div class="col-lg-8 col-lg-offset-2 text-center">
		<p>Foto</p>
		<input id="editEventFile" onchange="setclickImage(this,'edit')" style="display: none;" type="file" name="file">
		<img id="editEventImage" onclick="clickImage('edit')" src=" <?php echo $data[0]['photo']!=''? $data[0]['photo'] : 'img/imgDefault.jpg'; ?>" style="width: 100%; height: auto;">
	</div> 
	<div class="col-lg-8 col-lg-offset-2 text-center"><br></div>
<?php
}
?>
	<div style="margin-top: 20px;" class="col-lg-8 col-lg-offset-2 text-center">
		<h1>Modifica evento</h1>
		<hr class="light">
		<b style="color: black;" id="error"></b> <br><br>
	</div>
	<div class="col-lg-8 col-lg-offset-2 text-center">		
			<div class="col-md-4 text-center">
				<p>Info</p>
				<input type="hidden" name="id" value="<?php echo $data[0]['id']; ?>">
				<p><input value="<?php echo $data[0]['title']; ?>" placeholder="Titolo" class="input-sm" type="text" name="title"></p>
				<p><textarea style="border-radius: 10px; max-width: 100%;" placeholder="Descrizione" class="input-sm" type="password" name="description"><?php echo $data[0]['description']; ?></textarea></p>
				<p>Categoria</p>
				<p>
					<select id="category" class="input-sm" name="category">
						<?php
						$categories = new Categories();
						foreach ($categories->get() as $id => $category) {
						?>
						<option <?=$data[0]['category'] == $id ? ' selected="selected"' : '';?> value="<?php echo $id; ?>"><?php echo $category['title']; ?></option>
						<?php
						}
						?>
					</select>
				</p>
			</div>
			<div class="col-md-4 text-center">
				<p>Dove</p>
				<p><input value="<?php echo $data[0]['local']; ?>" class="input-sm" name="local" placeholder="Nome locale" type="text"/></p>
				<p><input value="<?php echo $data[0]['place']; ?>" class="input-sm" name="place" type="text" id="us2-address"/></p>
				<p><div style="width: 100%; height: 200px;" id="us2" ></div></p>
				<input name="latitude" type="hidden" id="us2-lat"/>
				<input name="longitude" type="hidden" id="us2-lon"/>

				<p>Quando</p>
				<p><input value="<?php echo $date_time[0]; ?>" placeholder="Data" id="datepickerResult" class="input-sm" type="text" name="date" ></p>
				<p><div style="background-color: white; color: black; width:220px; margin-left: calc(50% - 110px); border-radius: 10px;" id="datepicker"></div></p>
				<p><input value="<?php echo $date_time[1]; ?>" id="timepicker" data-default-time="false" placeholder="Ore" class="input-sm" type="text" name="time"></p>
			</div>
			<div class="col-md-4 text-center">
				<p>Numero utenti massimo</p>
				<p><input value="<?php echo $data[0]['max']; ?>" placeholder="Vuoto per invitati ∞" class="input-sm" type="text" name="max"></p>
				<p>Tipo</p>
				<p>
					<select class="input-sm" name="type">
						<option <?=$data[0]['type'] == 0 ? ' selected="selected"' : '';?> value="0">Pubblico</option>
						<option <?=$data[0]['type'] == 1 ? ' selected="selected"' : '';?> value="1">Privato</option>
					</select>
				</p>
			</div>
	</div>
		
	<div style="margin-top: 20px;" class="col-md-12 text-center">
		<p><button id="publishBtn" class="btn btn-default btn-xl" type="submit">Aggiorna</button>  <a href="javascript:;" onclick="deleteEvent(<?php echo $data[0]['id'] ?>);" class="btn btn-danger btn-xl">Elimina</a></p>
	</div>
	
</form>

	
	<script>
	$("#editevent").submit(function() {
		$.ajax({
			type: "POST",
			url: "events/edit.php",
			data: new FormData($(this)[0]),
			cache: false,
			contentType: false,
			processData: false,
			success: function(data){
				$("#publishBtn").html("Pubblica");
				var result = jQuery.parseJSON(data);
				if (result.editevent==true) {
					$("#news").html('<div class="col-lg-8 col-lg-offset-2 text-center"><h1>Modificato</h1></div');
					scrollTo("#News");
				} else {
					$("#error").html(result.editevent);
				}
			},beforeSend: function() {
				loading("#publishBtn");
			},
		});
		return false;
	});
	$(function(){
		$('#datepicker').datepicker('update', new Date(<?php echo '"'.$time.'"'; ?>));
		$('#us2').locationpicker({
			location: {latitude: <?php echo $data[0]['latitude']; ?>, longitude: <?php echo $data[0]['longitude']; ?>},	
			radius: 1,
			enableAutocomplete: true,
			inputBinding: {
		        latitudeInput: $('#us2-lat'),
		        longitudeInput: $('#us2-lon'),
		        locationNameInput: $('#us2-address')
		    }
		});
	});

	</script> 
<?php
} else {
?>
	<script>Reload();</script>
<?php
}
?>