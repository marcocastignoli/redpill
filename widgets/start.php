<?php
require_once "../lib/config.php";
if (userLogged()) {
?>
	
	<div onclick="Search();" class="service-box widget" style="display: inline-block;">
        <i class="fa fa-4x fa-search wow bounceIn text-primary" data-wow-delay=".1s"></i>
        <h3 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" class="title">Cerca</h3>
        <p style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" class="text-muted">Cerca gli eventi vicino a te!</p>
    </div>
	
	
	<div  onclick="News(true)" class="service-box widget" style="display: inline-block;">
        <i class="fa fa-4x fa-newspaper-o wow bounceIn text-primary" data-wow-delay=".2s"></i>
        <h3 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" class="title">Nuovo</h3>
        <p style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;" class="text-muted">Crea un nuovo evento!</p>
    </div>
	
<?php
} else {
?>
	<script>Login();</script>
<?php
}
?>