<?php
require_once "../lib/config.php";
require_once "../notifications/Notification.php";

$userID = $_POST['u'];
$eventID = $_POST['e'];

$data["acceptRequest"]=null;

if (userLogged()) {
	if (isAuthor($eventID)) {
		$database->update("requests", [
			"status" => "1"
		],[
			"AND" => [
				"id_event" => $eventID,
				"id_user" => $userID,
			]
		]);
		//invia notifica della richiesta accettata
		$event=$database->get("events", "*", [
			"id" => $eventID,
		]);
		$acceptedNoti = new Notification();
		$acceptedNoti->user=$userID;
		$acceptedNoti->title="Richiesta accettata";
		$acceptedNoti->description="Ora puoi vedere i dettagli dell'evento \"".$event['title']."\"";
		$acceptedNoti->link="Event(".$eventID.")";
		$acceptedNoti->fa="";
		$acceptedNoti->send($_SESSION['User']->id,"");

		$data["acceptRequest"]=true;
	} else {
		$data["acceptRequest"]="Accettarsi le richieste da solo? Uno psicologo?";
	}
}
echo json_encode($data);
?>