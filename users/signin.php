<?php
require_once "../lib/config.php";
require_once "../notifications/Notification.php";

$username=$_POST['username'];
$password=$_POST['password'];
$name=$_POST['name'];
$surname=$_POST['surname'];
$place=$_POST['place'];
$longitude=$_POST['longitude'];
$latitude=$_POST['latitude'];
$email=$_POST['email'];

//errors
$data["signin"]=null;

if ($username!="" && $password!="" && $name!="" && $surname!="" && $place!="" && $email!="") {
	//controllo l'username
	if (ctype_alnum ($username)) {
		//controllo se username esiste
		$count = $database->count("users", [
			"username" => $username
		]);

		if ($count==0) {
			$id = $database->insert("users", [
				"username" => $username,
				"password" => sha1($password),
				"name" => $name,
				"surname" => $surname,
				"place" => $place,
				"longitude" => $longitude,
				"latitude" => $latitude,
				"email" => $email,
			]);
			$data["e"]=$_POST['idEvent'];
			$data["signin"]=true;
		} else {
			$data["signin"]="Username already exists.";
		}
	} else {
		$data["signin"]="Puoi usare solamente caratteri alfanumerici.";
	}
} else {
	$data["signin"]="Devi compilare tutti i campi.";
}
echo json_encode($data);
?>