<?php
require_once "../lib/config.php";

@$category = $_POST['c'];
@$page=$_POST['p'];
@$location=$_POST['l'];
@$distance=$_POST['d'];
@$when=$_POST['w'];
if (!isset($distance)) {
	$distance=10;
}
if (!isset($page)) {
	$page=0;
}

$latitude=null;
$longitude=null;

if (isset($location)) {
	$latitude=$location['latitude'];
	$longitude=$location['longitude'];
} else {
	$latitude=$_SESSION['User']->latitude;
	$longitude=$_SESSION['User']->longitude;
}

date_default_timezone_get();
$date = null;
if ($when!=0) {
	$date = date('Y-m-d', time());
	$date=date('Y-m-d G:i', strtotime($date . ' + '.$when.' day'));
} else {
	$date = date('Y-m-d G:i', time());
}
$sessionID= isset($_SESSION['User']->id) ? $_SESSION['User']->id : 0;

$data=null;

if ($category==null) {
	$data = $database->query("	SELECT events.id,events.author,users.name as name,users.surname as surname,events.title,events.description,events.place,events.latitude,events.longitude,events.date,events.max,events.category,events.type, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_SESSION['User']->id." AND id_event = events.id) as status
								FROM events 
								INNER JOIN users ON events.author = users.id
								HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', events.latitude, events.longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
								ORDER BY events.date ASC
								LIMIT 8 OFFSET $page
	")->fetchAll();
} else {
	$data = $database->query("	SELECT id,author,title,description,place,latitude,longitude,date,max,category,type, (select count(*) from requests where status = 1 AND id_event = events.id) as requestsCount, (select status from requests where id_user = ".$_SESSION['User']->id." AND id_event = events.id) as status
								FROM events 
								WHERE category = $category 
								HAVING (events.max=0 OR events.max > requestsCount) AND (GeoDistDiff('km', latitude, longitude, $latitude, $longitude) <= $distance ) AND cast(events.date as datetime) >= cast('$date' as datetime)
								ORDER BY date ASC
								LIMIT 8 OFFSET $page
	")->fetchAll();
}

//sistemo array
foreach ($data as $k => $event) {
	foreach ($event as $key => $value) {

	    if (is_int($key)) {
	        unset($data[$k][$key]);
	    }
	}	
}
echo json_encode($data);