<?php
require_once "../lib/config.php";
require_once "../notifications/Notification.php";

$username=$_POST['username'];
$password=$_POST['password'];

$info = $database->select("users", "*" , [
	"AND" => [
		"username" => $username,
		"password" => sha1($password),
	]
]);
$data["login"]=false;
$data["e"]=null;
if ($info!=null) {
	$user = new User();
	$user->id=$info[0]['id'];
	$user->username=$info[0]['username'];
	$user->password=$info[0]['password'];
	$user->name=$info[0]['name'];
	$user->surname=$info[0]['surname'];
	$user->place=$info[0]['place'];
	$user->latitude=$info[0]['latitude'];
	$user->longitude=$info[0]['longitude'];
	$user->premium=$info[0]['premium'];
	$user->email=$info[0]['email'];
	
	$_SESSION["User"] = $user;
	if (key_exists("fb", $info[0])) {
		$_SESSION["FBID"] = true;
	}
	
	if (isset($_POST['idEvent']) && $_POST['idEvent']!="") {
		$data["e"]=$_POST['idEvent'];
	}
	$data["login"]=true;
} else {
	$data["login"]=false;
}
echo json_encode($data);

?>