<?php
require_once "../lib/config.php";
require_once "connection.php";
require_once "UI.php";

if (userLogged()) {
	global $wolfood;
	$data = mssql_query(" 	SELECT * FROM redpill
							WHERE  id_dp='".$database->quote($_SESSION['User']->id)."'");
	$row = mssql_fetch_array($data);
	//check se l'utente non ha già inviato una richiesta
	if (count($row)==0) {
		//inserisco nella tabella su wolfood
		$check = mssql_query("	INSERT INTO redpill (id_wf,id_dp,pin)
								VALUES (".$_POST['id_wf'].",".$database->quote($_SESSION['User']->id).",LPAD(FLOOR(RAND() * 10000), 4, '0'));");
		//ritorno conferma creazione dico all'utente di inserire il pin che trova su wolfood con possibilità di annullare
		if ($check) {
		?>
			<p>Accedi a Wolfood e accetta la connessione, altrimenti:</p>
			<button onclick="undoWolfoodPin();" class="btn btn-danger btn-sm">ANNULLA</button>
			<script>
				function undoWolfoodPin(){
					$.ajax({
						type: "POST",
						url: "wolfood/undo.php",
						success: function(data){
							if (data==true) {
								$("#wolfood").html(<?php connectWolfood(); ?>);
							}
						}
					});
					return false;
				}
			</script>
		<?php
		}else {
			?>
			Errore!
			<?php
		}
		
	}
}
?>