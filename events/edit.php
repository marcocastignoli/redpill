<?php
require_once "../lib/config.php";
require_once "../images/Images.php";

function validateDate($date, $format = 'Y-m-d H:i:s'){
    $d = DateTime::createFromFormat($format, $date);
    return $d && $d->format($format) == $date;
}

if (userLogged()) {
	$id=$_POST['id'];
	$title=$_POST['title'];
	$description=$_POST['description'];
	$place=$_POST['place'];
	$latitude=$_POST['latitude'];
	$longitude=$_POST['longitude'];
	$date=$_POST['date'];
	$time=$_POST['time'];
	$max=$_POST['max'];
	$category=$_POST['category'];
	$type=$_POST['type'];
	$local=$_POST['local'];

if (isAuthor($id)) {
	//errors
	$data["editevent"]=null;

	if ($title!="" && $description!="" && $place!="" && $date!="" && $category!="" && $type!="" && $local!="") {
		
		$datetime = new DateTime($date);
		$max=(int)$max;
		if ($max>=0) {
			if(validateDate($datetime->format('Y-m-d'),'Y-m-d')){
				$time=explode( ':', $time );
				if ((is_numeric($time[0]) && (int)$time[0]>=0 && (int)$time[0]<=23) && ( is_numeric($time[1]) && (int)$time[1]>=0 && (int)$time[1]<=59)) {
					$datetime->setTime($time[0],$time[1],"00");
					$categories = new Categories();
					if ($category>=0 && $category <$categories->cou() ) {
						if ($type==0 || $type ==1) {
							if ($max!=1) {
								$database->update("events", [
									"title" => $title,
									"description" => $description,
									"place" => $place,
									"date" => $datetime->format('Y-m-d H:i:s'),
									"max" => $max,
									"category" => $category,
									"type" => $type,
									"latitude" => $latitude,
									"longitude" => $longitude,
									"local" => $local,
								],[
									"id"=>$id,
								]);
								$data["editevent"]=true;
								if ($_FILES['file']['name']!="") {
									$checkPhoto=addPhotoEvent($_FILES['file'],$id);
									if (!$checkPhoto) {
										$data["editevent"]="Problemi con il caricamento della foto.";
									}
								}
								
							} else {
								$data["editevent"]="Non puoi creare un evento al quale partecipi soltanto te";
							}
						} else {
							$data["editevent"]="Controlla il tipo";
						}
					} else {
						$data["editevent"]="Controlla la categoria";
					}
				} else {
					$data["editevent"]="Controlla l'orario";
				}
			} else {
				$data["editevent"]="Controlla la data";
			}
		} else {
			$data["editevent"]="Il numero di persone non può essere negativo.";
		}
		
	} else {
		$data["editevent"]="Completa tutti i campi";
	}
} else {
	$data["editevent"]="Stai per caso modificando l'evento di qualcun altro?";
}
echo json_encode($data);
}
?>