<?php
require_once "../lib/config.php";
if (!userLogged()) {
	$idEvent=@$_POST['e'];
?>
	<h1 style="text-shadow: 0px 0px 5px rgba(0,0,0,0.8) !important;">Registrati</h1>
	<hr>
	<form id="signin">
		<b style="color: #f05f40;" id="error"></b><br><br>
		<p><input placeholder="Email" class="input-sm" type="text" name="email"></p>
		<p><input placeholder="Username" class="input-sm" type="text" name="username"></p>
		<p><input placeholder="Password" class="input-sm" type="password" name="password"></p>
		<p><input placeholder="Nome" class="input-sm" type="text" name="name"></p>
		<p><input placeholder="Cognome" class="input-sm" type="text" name="surname"></p>
		<p><input class="input-sm" name="place" type="text" id="us1-address"/></p>
		<p><div style="width: 100%; height: 200px;" id="us1" ></div></p>
		<input name="latitude" type="hidden" id="us1-lat"/>
		<input name="longitude" type="hidden" id="us1-lon"/>
		<input type="hidden" name="idEvent" value="<?php echo $idEvent; ?>">
		<p><button class="btn btn-primary btn-xl" type="submit">Registrati</button></p>
	</form>
	<script> 
	$("#signin").submit(function() {
		$.ajax({
			type: "POST",
			url: "users/signin.php",
			data: $("#signin").serialize(),
			success: function(data){
				var result = jQuery.parseJSON(data);
				if (result.signin==true) {
					if (result.e!=null) {
						Login(result.e);
					} else {
						Reload();
					}
				} else {
					$("#error").html(result.signin);
				}
			}
		});
		return false;
	}); 
	</script> 
<?php
} else {
?>
	<script>Reload();</script>
<?php
}
?>