<?php
require_once 'medoo.min.php';
ini_set('session.gc_maxlifetime', 3600*24*30);
session_set_cookie_params(3600*24*30);
session_start();
$database = new medoo([
	'database_type' => 'mysql',
	'database_name' => 'friends',
	'server' => 'localhost',
	'username' => 'Envious',
	'password' => 'password',
	'charset' => 'utf8'
]);

class User {
	public $id = NULL;
	public $name = NULL;
	public $password = NULL;
	public $image = NULL;
}

function userLogged(){
	global $database;
	if (isset($_SESSION["User"])) {
		$user= $_SESSION["User"];
		$info = $database->select("users", [
			"name",
			"password",
		], [
			//condizioni
			"name[=]" => $user->name,
			"password[=]" => $user->password,
		]);
		if (isset($info)) {
			return true;
		} else{
			return false;
		}
	} else {
		return false;
	}
}

function isAuthor($id){
	global $database;
	$idUtente = $database->get("events","author", [
		"id"=>$id,
	]);
	if($idUtente==$_SESSION['User']->id){
		return true;
	}else{
		return false;
	}
}

class Categories {
	private $elements=[
		0 => [
			"title"=>"Sport di squadra",
			"description"=>"Divertiti con tanti amici, organizza sfide emozionanti!",
			"icon"=>"fa-futbol-o"
		],
		1 => [
			"title"=>"Giochi di ruolo",
			"description"=>"Nerd? Questo è il tuo luogo ideale! Fai volare la tua immaginazione...",
			"icon"=>"fa-magic"
		],
		2 => [
			"title"=>"Giochi da tavolo",
			"description"=>"Fai correre il cervello, trova un rivale alla tua altezza!",
			"icon"=>"fa-table"
		],
		3 => [
			"title"=>"Studio di squadra",
			"description"=>"Giornata di sole e chiuso in casa a studiare? Fatti furbo e impara in compagnia!",
			"icon"=>"fa-book"
		],
		4 => [
			"title"=>"Indoor",
			"description"=>"Si può sudare anche in casa.",
			"icon"=>"fa-home"
		],
		5 => [
			"title"=>"Festini",
			"description"=>"Chi ha mai detto che ci si diverte solo nei ballabili?",
			"icon"=>"fa-beer"
		],
		6 => [
			"title"=>"Discoteche",
			"description"=>"Trova i migliori locali nei tuoi dintorni, balla fino allo sfinimento!",
			"icon"=>"fa-glass"
		],
	];
	public function get(){
		return $this->elements;
	}
	public function cou(){
		return count($this->elements);
	}
}


?>
